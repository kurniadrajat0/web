<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web Routes for your application. These
| Routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* 
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
*/

use App\Http\Controllers\PesertaController;
use App\Http\Controllers\DropzoneController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\adminController;

Route::get('/', [
    'as' => 'landing-page',
    'uses' => 'HomeController@landing_page'
]);

Route::match(['post', 'get'], '/login', [
    'as' => 'login',
    'uses' => 'HomeController@login'
]);

Route::match(['post', 'get'], '/registrasi', [
    'as' => 'registrasi',
    'uses' => 'HomeController@registrasi'
]);

Route::get('/registrasi/verifikasi/{tipe}/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'HomeController@confirm'
]);

Route::get('/resend/{type}/{email}', [
    'as'    => 'resend',
    'uses'  => 'HomeController@reSendEmail'
]);

Route::group(['middleware'=>'auth:peserta', 'as'=>'user.'], function () {
    Route::get('/dashboard',  [PesertaController::class, 'dashboard'])->name('dashboard');

    Route::get(
        '/pengumuman', 
        [PesertaController::class, 'view_pengumuman']
    )->name('pengumuman');

    Route::get(
        '/email', 
        [PesertaController::class, 'send_email']
    )->name('email');

    Route::match(['post', 'get'], '/profile', [
        'as' => 'profile',
        'uses' => 'PesertaController@update_profile'
    ]);

    Route::get(
        '/tata-cara-pendaftaran', 
        [PesertaController::class, 'view_tata_cara_pendaftaran']
    )->name('tata-cara-pendaftaran');

    Route::get(
        '/kontak', 
        [PesertaController::class, 'view_kontak']
    )->name('kontak');

    Route::get(
        '/sertifikat', 
        [PesertaController::class, 'view_list_sertifikat']
    )->name('list-sertifikat');

    Route::get(
        '/sertifikat/download/{id}', 
        [PesertaController::class, 'download_sertif']
    )->name('download-sertifikat');

    Route::get(
        '/file-lkti', 
        [DropzoneController::class, 'index']
    )->name('lkti-view');

    Route::post(
        '/file-lkti/upload', 
        [DropzoneController::class, 'store']
    )->name('lkti-upload');

    Route::post(
        '/file-lkti/delete', 
        [DropzoneController::class, 'destroy']
    )->name('lkti-delete');
    
    Route::match(['post', 'get'], '/pilih-kontes', [
        'as' => 'pilih-kontes',
        'uses' => 'PesertaController@pilih_kontes'
    ]);

    Route::match(['post', 'get'], '/pendaftaran-tim', [
        'as' => 'daftarkan-tim',
        'uses' => 'PesertaController@daftarkan_tim'
    ]);


    Route::match(['post', 'get'], '/konfirmasi-pembayaran', [
        'as' => 'konfirmasi-pembayaran',
        'uses' => 'PesertaController@konfirmasi_pembayaran'
    ]);

    Route::get(
        '/status-pendaftaran', 
        [PesertaController::class, 'status_pendaftaran']
        )->name('status-pendaftaran');

        Route::get(
            '/kunci-data', 
            [PesertaController::class, 'kunci_data']
        )->name('kunci-data');

    Route::post(
        '/lihat-kontes', 
        [PesertaController::class, 'lihat_kontes']
        )->name('lihat-kontes');

    Route::match(['post', 'get'], '/cetak_kartu', [
        'as' => 'cetak-kartu',
        'uses' => 'PesertaController@cetak_kartu'
    ]);

    Route::get('/logout', 'PesertaController@logout')->name('logout');
});

/* 
|--------------------------------------------------------------------------
| End User Routes
|--------------------------------------------------------------------------
*/


/* 
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
Route::prefix('admin')->group(function () {
    /* Route REGIS LOGIN*/
    
    Route::match(['post', 'get'], '/login', [
        'as' => 'login-admin',
        'uses' => 'adminController@login'
    ]);
    
    Route::match(['post', 'get'], '/registrasi', [
        'as' => 'registrasi-admin',
        'uses' => 'adminController@registrasi'
    ]);


Route::group(['middleware'=>'auth:admin'], function () {
    Route::get('/beranda',function(){
        return view('Dashboard.beranda_admin'); #nampilin blade beranda
    });
    
    Route::get('/','berandaController@index')->name('beranda-admin');
    /*Contoh ajax route */
    /*Route::get('product-list', [ProductController::class, 'index']);
    Route::get('product-list/{id}/edit', [ProductController::class, 'edit']);
    Route::post('product-list/store', [ProductController::class, 'store']);
    Route::get('product-list/delete/{id}', [ProductController::class, 'destroy']);*/
    
    /*Route Email */
    Route::get(
        '/email', 
        [emailController::class, 'send_email']
    )->name('email');

    /*Route Data Team */
    Route::get('/data-team','teamController@index')->name('data-team');


    /*Route Peserta/User*/
    Route::get('/data-user','userController@index')->name('data-user');
    //edit/update
    Route::get('/edit-user/{id}','userController@edit')->name('edit-user');
        
    Route::post('/update-user/{id}','userController@update')->name('update-user');

    /* Route transaksi */
    //create
    Route::get('/transaksi-baru','purchaseController@index')->name('transaksi-baru');
    Route::get('/transaksi-disetujui','purchaseController@index_disetujui')->name('transaksi-disetujui');
    Route::get('/transaksi-ditolak','purchaseController@index_ditolak')->name('transaksi-ditolak');

    //edit/update
    Route::get('/edit-transaksi/{id}','purchaseController@edit')->name('edit-transaksi');
    
    Route::post('/update-transaksi/{id}','purchaseController@update')->name('update-transaksi');

    //update status pembayaran
    Route::get('/update-statusPembayaran/{id}','purchaseController@update_statusPembayaran')->name('update-statusPembayaran');

    //delete
    Route::get('/delete-transaksi/{id}','purchaseController@destroy')->name('delete-transaksi');


    /* Route KONTES */
    //create
    Route::get('/data-kontes','kontesController@index')->name('data-kontes');
    
    Route::get('/create-kontes','kontesController@create')->name('create-kontes');
    
    Route::post('/simpan-kontes','kontesController@store')->name('simpan-kontes');

    //update
    Route::get('/update-statusKontes/{id}','kontesController@update_statusKontes')->name('update-statusKontes');

    Route::get('/update-jenisKontes/{id}','kontesController@update_jenisKontes')->name('update-jenisKontes');
    
    //edit/update
    Route::get('/edit-kontes/{id}','kontesController@edit')->name('edit-kontes');
    
    Route::post('/update-kontes/{id}','kontesController@update')->name('update-kontes');
    
    //delete
    Route::get('/delete-kontes/{id}','kontesController@destroy')->name('delete-kontes');
    
    /*Route TAHAPAN KONTES*/
    //create
    Route::get('/data-tahapan','tahapanController@index')->name('data-tahapan');
    //ajax tahapan
    Route::get('getdtahapan',[
        'uses'=>'tahapanController@getdtahapan',
        'as'=>'ajax.getdtahapan',
    ]);
    //ajax kontes
    Route::get('getdkontes',[
        'uses'=>'kontesController@getdkontes',
        'as'=>'ajax.getdkontes',
    ]);
    //ajax transaksi
    Route::get('getdtransaksi',[
        'uses'=>'purchaseController@getdtransaksi',
        'as'=>'ajax.getdtransaksi',
    ]);

    Route::get('getdtransaksi_ditolak',[
        'uses'=>'purchaseController@getdtransaksi_ditolak',
        'as'=>'ajax.getdtransaksi_ditolak',
    ]);

    Route::get('getdtransaksi_disetujui',[
        'uses'=>'purchaseController@getdtransaksi_disetujui',
        'as'=>'ajax.getdtransaksi_disetujui',
    ]);
    //ajax peserta
    Route::get('getdpeserta',[
        'uses'=>'userController@getdpeserta',
        'as'=>'ajax.getdpeserta',
    ]);
    //ajax data team
    Route::get('getdteam',[
        'uses'=>'teamController@getdteam',
        'as'=>'ajax.getdteam',
    ]);
    
    //create tahapan
    Route::get('/create-tahapan','tahapanController@create')->name('create-tahapan');
    
    Route::post('/simpan-tahapan','tahapanController@store')->name('simpan-tahapan');
    
    Route::get('/update-statusTahapan/{id}','tahapanController@update_statusTahapan')->name('update-statusTahapan');
    
    //edit/update
    Route::get('/edit-tahapan/{id}','tahapanController@edit')->name('edit-tahapan');
    
    Route::post('/update-tahapan/{id}','tahapanController@update')->name('update-tahapan');
    
    //delete
    Route::get('/delete-tahapan/{id}','tahapanController@destroy')->name('delete-tahapan');

    Route::get('/logout', 'adminController@logout')->name('logout');

});
}); 

/* 
|--------------------------------------------------------------------------
| End Admin Routes
|--------------------------------------------------------------------------
*/
