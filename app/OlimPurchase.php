<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OlimPurchase extends Model
{
    protected $table = 'olim_purchases';
    protected $fillable = [
        'id',
        'no_invoice',
        'peserta_id',
        'kontes_id',
        'tanggal_transaksi',
        'status_pembayaran',
        'unique_number',
        'total_tagihan',
        'metode_pembayaran',
        'tanggal_transfer',
        'file_bukti',
        'kode_voucher',
        'potongan_voucher',
        'kode_peserta',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasOne_peserta()
    {
        return $this->hasOne(Peserta::class,'id');
    }

    public function hasOne_kontes()
    {
        return $this->hasOne(Kontes::class,'id');
    }

    public function belongsto_data_team()
    {
        return $this->belongsTo(OlimDataTeam::class, 'peserta_id');
    }
}
