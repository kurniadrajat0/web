<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontes extends Model
{
    protected $table ="kontes";
    protected $primaryKey ="id";
    protected $fillable=[
        'id','nama_kontes', 'jenis_kontes', 'deskripsi_kontes',
        'tahapan_kontesid','tgl_kontes','harga_kontes','link_sertifikat',
        'status_kontes','pengumuman_kontes',
    ];
    /**
     * Set the jenis kontes
     *
     */
    //buat one to many
    public function belongsto_tahapan()
    {
        return $this->belongsTo(tahapanKontes::class,'tahapan_kontesid'); 
    }

    public function belongsto_purchase()
    {
        return $this->belongsTo(OlimPurchase::class, 'kontes_id');
    }

    public function belongsto_data_team()
    {
        return $this->belongsTo(OlimDataTeam::class, 'kontes_id');
    }

}
