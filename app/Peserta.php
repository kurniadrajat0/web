<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Peserta extends Authenticatable
{
    protected $table = 'pesertas';
    protected $fillable = [
        'id',
        'email',
        'password',
        'type_login',
        'email_verification',
        'active',
        'notif',
        'confirmation_code',
        'token_google',
        'referal',
        'kode_referal_saya',
        'api_token',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * One to Many : Kontes
    */
    public function get_purchase()
    {
        return $this->belongsTo(OlimPurchase::class, 'peserta_id');
    }

     /**
     * One to Many : Kontes
    */
    public function get_data_team()
    {
        return $this->belongsTo(OlimDataTeam::class, 'peserta_id');
    }
}
