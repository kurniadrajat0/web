<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OlimDataTeam extends Model
{
    //protected $table = 'olim_purchases';
    protected $fillable = [
        'id',
        'peserta_id',
        'data_tim',
        'kontes_id',
        'status_data',
        'status_pembayaran',
        'status_lolos',
    ];

    public function get_peserta()
    {
        return $this->hasOne(Peserta::class,'id');
    }

    public function get_kontes()
    {
        return $this->hasOne(Kontes::class,'id');
    }

    public function get_purchase()
    {
        return $this->hasOne(OlimPurchase::class,'peserta_id');
    }
}
