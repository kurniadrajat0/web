<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PesertaController;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Kontes;
use App\OlimDataTeam;
use App\OlimPurchase;
use App\Peserta;
use Illuminate\Support\Facades\Validator;
use DB;
use DataTables;
use Illuminate\Support\Facades\DB as FacadesDB;
use Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;


class purchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtransaksi=OlimPurchase::with('hasOne_peserta','hasOne_kontes')->latest()->paginate(10);
        return view('Transaksi.transaksi_baru',compact('dtransaksi'));
    }

    public function index_disetujui()
    {
        $dtransaksi=OlimPurchase::with('hasOne_peserta','hasOne_kontes')->latest()->paginate(10);
        return view('Transaksi.transaksi_disetujui',compact('dtransaksi'));
    }

    public function index_ditolak()
    {
        $dtransaksi=OlimPurchase::with('hasOne_peserta','hasOne_kontes')->latest()->paginate(10);
        return view('Transaksi.transaksi_ditolak',compact('dtransaksi'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $epurchase=OlimPurchase::all();
        return view('Kontes.create_transaksi',compact('epurchase'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        switch ($request->input('action')) {
            case 'transaksi_baru':
                // Save model
                break;
    
            case 'transaksi_disetujui':
                // Preview model
                break;
    
            case 'transaksi_ditolak':
                // Redirect to advanced edit
                break;
        }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ekontes=Kontes::all();
        $epurchase=OlimPurchase::findorfail($id);//buat manggil seluruh variable jabatan
        $eolimdatateam=OlimDataTeam::findorfail($id);
        $url = asset('storage/images/'.$epurchase->file_bukti); //terimpan di public storage images
        return view('Transaksi.edit_transaksi',compact('epurchase','ekontes','eolimdatateam','url')); #ceompact berfungsi agar variable $peg bisa di pakai di halaman Edit-pegawais
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status_pembayaran' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        
        $status_pembayaran =$_POST['status_pembayaran'];
      
        $epurchase = OlimPurchase::findorfail($id);
        $datateam = OlimDataTeam::where('peserta_id',$epurchase->peserta_id)->first();
       
        $epurchase->status_pembayaran = $status_pembayaran;
        $datateam->status_pembayaran = $status_pembayaran;

        $kontes         = Kontes::where('id',$epurchase->peserta_id)->first();

        $peserta = Peserta::where('id',$epurchase->peserta_id)->first();

        $data = DB::table('olim_purchases as op')
        ->join('pesertas as p', 'op.peserta_id', '=', 'p.id')
        ->join('kontes as k', 'op.kontes_id', '=', 'k.id')
        ->select('*')
        ->get(); //tadinya first
        
        $epurchase->save();
        $datateam->save();

        
        if ($epurchase->status_pembayaran==1 && $datateam->status_pembayaran ==1){
            Mail::send('Email.email_konfirmasi', ['data'=>$data], function ($message) use($peserta) {
                $message->from('admin@campuspedia.id', 'Campuspedia Team');
                $message->sender('admin@campuspedia.id', 'Campuspedia Team');
                $message->to($peserta->email);
                
                $message->subject('Status Pembayaran Peserta - Olimpiade Campuspedia');
            });
        }

        return redirect('admin/transaksi-baru')->with('toast_success', 'Data Berhasil Di Update!',['kontes'=>$kontes, 'purchase'=>$epurchase, 'peserta'=>$peserta]);
    }

    public function send_email(){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $purchase    = OlimPurchase::where('peserta_id', $peserta_id)->first();

        return view('Email.email_konfirmasi')->with(['purchase'=>$purchase]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $epurchase=OlimPurchase::findorfail($id);//Model::findorfail //berfungsi untuk mencari data id menggunakan where agar tidak error jika data tidak ada datanya
        $epurchase->delete();
        return back()->with('info', 'Data Berhasil Di delete!');
    }
    public function update_statusPembayaran($id){
        $get_statusPembayaran=DB::table('olim_purchases')->select('status_pembayaran')->where('id','=',$id)->first();

        #check status
        if($get_statusPembayaran->status_pembayaran=='1'){
            $status_pembayaran='0';}
        else{
            $status_pembayaran='1';
        }
        #update jenis
        $values=array('status_pembayaran'=>$status_pembayaran);
        DB::table('olim_purchases')->where('id',$id)->update($values);
        return redirect('admin/data-transaksi')->with('toast_success', 'Jenis kontes Berhasil Di Update!');
    }
    public function getdtransaksi(){
        $getdtransaksi = DB::table('olim_purchases')
        ->join('pesertas', 'pesertas.id', '=', 'olim_purchases.peserta_id')
        ->join('kontes','kontes.id','=','olim_purchases.kontes_id')
        ->select('olim_purchases.*','pesertas.nama_lengkap as nama_lengkap','kontes.jenis_kontes as jenis_kontes')
        ->get(); //ubah disini 
        return \DataTables::of($getdtransaksi)
        ->addColumn('id',function($s){
            return '<td>'.$s->peserta_id.'</td>';  
        })
        ->addColumn('nama_peserta',function($s){
            return '<td>'.$s->nama_lengkap.'</td>';  
        })
        ->addColumn('jenis_kontes',function($s){
                if ($s->jenis_kontes == 1) {
                    return '<td> LKTI </td>';  
                } else {
                    return '<td> Olimpiade </td>';  
                }
        })
        ->addColumn('tanggal_transaksi',function($s){
            return '<td>'.$s->tanggal_transaksi.'</td>';  
        })
        ->addColumn('status_pembayaran',function($s){
            if($s->status_pembayaran=='-1'){
                return 'Pending/Baru';                        
            }  
            else if($s->status_pembayaran=='0'){
                return 'Belum lunas/Ditolak';
            }    
            else{
                return 'Lunas/Disetujui';
            }     
        })
        ->addColumn('metode_pembayaran',function($s){
            return '<td>'.$s->metode_pembayaran.'</td>';  
        })
        /*->addColumn('file_bukti',function($kat){
            $url = Storage::url($kat->file_bukti); //terimpan di public storage images
            $gambar = '<img src="'.$url.'">';
            return $gambar;
        })*/
        ->addColumn('aksi',function($s){
            return '<a target="_blank" href="/admin/edit-transaksi/'.$s->id.'" class="btn btn-primary"> View</a>';
        })
        ->rawColumns(['id','nama_peserta','jenis_kontes','tanggal_transaksi','status_pembayaran','total_tagihan','metode_pembayaran','aksi'])
        ->toJson();
    }

    public function getdtransaksi_ditolak(){
        $getdtransaksi_ditolak = DB::table('olim_purchases')
        ->where('status_pembayaran','=',0)
        ->join('pesertas', 'pesertas.id', '=', 'olim_purchases.peserta_id')
        ->join('kontes','kontes.id','=','olim_purchases.kontes_id')
        ->select('olim_purchases.*','pesertas.nama_lengkap as nama_lengkap','kontes.jenis_kontes as jenis_kontes')
        ->get(); //ubah disini 
        return \DataTables::of($getdtransaksi_ditolak)
        ->addColumn('id',function($s){
            return '<td>'.$s->peserta_id.'</td>';  
        })
        ->addColumn('nama_peserta',function($s){
            return '<td>'.$s->nama_lengkap.'</td>';  
        })
        ->addColumn('jenis_kontes',function($s){
            if ($s->jenis_kontes == 1) {
                return '<td> LKTI </td>';  
            } else {
                return '<td> Olimpiade </td>';  
            }  
        })
        ->addColumn('tanggal_transaksi',function($s){
            return '<td>'.$s->tanggal_transaksi.'</td>';  
        })
        ->addColumn('status_pembayaran',function($s){
            if($s->status_pembayaran=='-1'){
                return 'Pending/Baru';                        
            }  
            else if($s->status_pembayaran=='0'){
                return 'Belum lunas/Ditolak';
            }    
            else{
                return 'Lunas/Disetujui';
            }     
        })
        ->addColumn('metode_pembayaran',function($s){
            return '<td>'.$s->metode_pembayaran.'</td>';  
        })
        ->addColumn('aksi',function($s){
            return '<a target="_blank" href="/admin/edit-transaksi/'.$s->id.'" class="btn btn-primary"> View</a>';
        })
        ->rawColumns(['id','nama_peserta','jenis_kontes','tanggal_transaksi','status_pembayaran','total_tagihan','metode_pembayaran','aksi'])
        ->toJson();
    }

    public function getdtransaksi_disetujui(){
        $getdtransaksi_disetujui = DB::table('olim_purchases')
        ->where('status_pembayaran','=',1)
        ->join('pesertas', 'pesertas.id', '=', 'olim_purchases.peserta_id')
        ->join('kontes','kontes.id','=','olim_purchases.kontes_id')
        ->select('olim_purchases.*','pesertas.nama_lengkap as nama_lengkap','kontes.jenis_kontes as jenis_kontes')
        ->get(); //ubah disini 
        return \DataTables::of($getdtransaksi_disetujui)
        ->addColumn('id',function($s){
            return '<td>'.$s->peserta_id.'</td>';  
        })
        ->addColumn('nama_peserta',function($s){
            return '<td>'.$s->nama_lengkap.'</td>';  
        })
        ->addColumn('jenis_kontes',function($s){
            if ($s->jenis_kontes == 1) {
                return '<td> LKTI </td>';  
            } else {
                return '<td> Olimpiade </td>';  
            }  
        })
        ->addColumn('tanggal_transaksi',function($s){
            return '<td>'.$s->tanggal_transaksi.'</td>';  
        })
        ->addColumn('status_pembayaran',function($s){
            if($s->status_pembayaran=='-1'){
                return 'Pending/Baru';                        
            }  
            else if($s->status_pembayaran=='0'){
                return 'Belum lunas/Ditolak';
            }    
            else{
                return 'Lunas/Disetujui';
            }     
        })
        ->addColumn('metode_pembayaran',function($s){
            return '<td>'.$s->metode_pembayaran.'</td>';  
        })
        ->addColumn('aksi',function($s){
            return '<a target="_blank" href="/admin/edit-transaksi/'.$s->id.'" class="btn btn-primary"> View</a>';
        })
        ->rawColumns(['id','nama_peserta','jenis_kontes','tanggal_transaksi','status_pembayaran','total_tagihan','metode_pembayaran','aksi'])
        ->toJson();
    }
}
