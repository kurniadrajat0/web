<?php

namespace App\Http\Controllers;

use App\Peserta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Kontes;
use App\OlimDataTeam;
use App\OlimMedia;
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator as FacadesValidator;

class DropzoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta = Peserta::where('id', $peserta_id)->first();
        $kontes_id = $_GET['kontes_id'];
        $kontes = Kontes::find($kontes_id);


        return view('User.upload-file-lkti', compact('peserta', 'kontes'));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $peserta_id = Auth::guard('peserta')->user()->id;
        
        if($req->hasFile('file')){
            $m = new OlimMedia();
            $destinationPath    = 'public/file_upload_lkti/'.$peserta_id;
            $file = $req->file('file');
            $originalName = $file->getClientOriginalName();
            $extension = $file->clientExtension();
            $fileName = time() . "_" . $peserta_id . '_' .$originalName;
            $urlFile = $file->storeAs($destinationPath, $fileName);

            $m->nama_media = $req->input('nama_media');
            $m->deskripsi_media = $req->input('deskripsi_media');
            $m->file_media = $urlFile;
            $m->status_media = 1;
            $m->peserta_id = $peserta_id;
            $m->kontes_id = $req->input('kontes_id');
            $m->lock_data = 0;
            $m->save();
        } else {
            return response()->json(['status'=>false, 'message'=>'Silakan upload file anda terlebih dahulu.']);
        }
        $pesan2="Hi ".Auth::guard('peserta')->user()->nama_peserta;
        $pesan2.="Berkas Anda sudah masuk disistem kami\n\n";
        $pesan2.="Jangan lupa mengunci data berkas jika sudah final \n";

        $pesan2.="Terimakasih\n";
        $pesan2.="*#Campuspedia Team*";
        // Mail::send('email.submit_berkas', ['user' => Auth::guard('peserta')->user()->nama_peserta ], function ($m){
        //     $m->from('admin@campuspedia.id', 'Campuspedia Team');
        //     $m->to(Auth::guard('peserta')->user()->email, Auth::guard('peserta')->user()->nama_peserta)->subject('INFO SUBMIT BERKAS - Olimpiade Campuspedia');
        // });
        return response()->json(['status'=>true,'message'=>'File berhasil disimpan']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return back()->with('info', 'File berhasil dihapus!');  //pas udah selesai nyimpen kembali ke data-pegawai ,di akhir tambahan sweet alert
    }
}
