<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Kontes;
use App\tahapanKontes;//manggil model
use App\Peserta;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Support\MessageBag;
use DB;
use DataTables;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\OlimPurchase;
use Redirect;

class kontesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dkontes=Kontes::with('belongsto_tahapan')->latest()->paginate(10);
        return view('Kontes.data_kontes',compact('dkontes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $etahap=tahapanKontes::all();
        return view('Kontes.create_kontes',compact('etahap'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kontes' => 'required',
            'jenis_kontes' => 'required',
            'deskripsi_kontes' => 'required',
            'tgl_kontes' => 'required',
            'tahapan_kontesid' => 'required',
            'harga_kontes' => 'required',
            'link_sertifikat' => 'required|image|max:2048',
            'status_kontes' => 'required',
            'pengumuman_kontes' => 'required',
        ]);
        $filenameWithExt = $request->file('link_sertifikat')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('link_sertifikat')->getClientOriginalExtension();
        $filenameSimpan = $filename.'_'.time().'.'.$extension;
        $path = $request->file('link_sertifikat')->storeAs('public/images', $filenameSimpan);

        $input=$request->all();
        $name = $request->input('tahapan_kontesid');
        
        $jenis_kontes = $input['jenis_kontes'];
        $input['jenis_kontes'] = $jenis_kontes;

        $status_kontes = $input['status_kontes'];
        $input['status_kontes'] = $status_kontes;
        $input['link_sertifikat']=$filenameSimpan;
        Kontes::create($input);
        
        return redirect('admin/data-kontes')->with('toast_success', 'Data Berhasil Di Buat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $etahap=tahapanKontes::all();
        $ekontes=Kontes::findorfail($id);//buat manggil seluruh variable jabatan
        $url = asset('storage/images/'.$ekontes->link_sertifikat); //terimpan di public storage images
        return view('Kontes.edit_kontes',compact('ekontes','etahap','url')); #ceompact berfungsi agar variable $peg bisa di pakai di halaman Edit-pegawais
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_kontes' => 'required',
            'jenis_kontes' => 'required',
            'deskripsi_kontes' => 'required',
            'tgl_kontes' => 'required',
            'tahapan_kontesid' => 'required',
            'harga_kontes' => 'required',
            'link_sertifikat' => 'required|image|max:2048',
            'status_kontes' => 'required',
            'pengumuman_kontes' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $nama =$_POST['nama_kontes'];

        $ekontes = Kontes::findorfail($id);
        $ekontes->nama_kontes = $nama;

        if ($request->file('link_sertifikat')) {

            $filenameWithExt = $request->file('link_sertifikat')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('link_sertifikat')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time().'.'.$extension;
            $path = $request->file('link_sertifikat')->storeAs('public/images', $filename);
            $ekontes->link_sertifikat=$filename;
        }
        $ekontes->save();

        return redirect('admin/data-kontes')->with('toast_success', 'Data Berhasil Di Update!');  //pas udah selesai nyimpen kembali ke data-pegawai ,di akhir tambahan sweet alert
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
    public function destroy($id)
    {
        $etahap=tahapanKontes::all();
        $ekontes=Kontes::findorfail($id);//Model::findorfail //berfungsi untuk mencari data id menggunakan where agar tidak error jika data tidak ada datanya
        $ekontes->delete();
        return back()->with('info', 'Data Berhasil Di delete!');  //pas udah selesai nyimpen kembali ke data-pegawai ,di akhir tambahan sweet alert
    }

    public function update_statusKontes($id){
        $get_status=DB::table('kontes')->select('status_kontes')->where('id','=',$id)->first();

        #check status
        if($get_status->status_kontes=='1'){
            $status_kontes='0';}
        else{
            $status_kontes='1';
        }
        #update status
        $values=array('status_kontes'=>$status_kontes);
        DB::table('kontes')->where('id',$id)->update($values);
        return redirect('admin/data-kontes')->with('toast_success', 'Status kontes Berhasil Di Update!');
    }
    public function update_jenisKontes($id){
        $get_jenis=DB::table('kontes')->select('jenis_kontes')->where('id','=',$id)->first();

        #check status
        if($get_jenis->jenis_kontes=='1'){
            $jenis_kontes='0';}
        else{
            $jenis_kontes='1';
        }
        #update jenis
        $values=array('jenis_kontes'=>$jenis_kontes);
        DB::table('kontes')->where('id',$id)->update($values);
        return redirect('admin/data-kontes')->with('toast_success', 'Jenis kontes Berhasil Di Update!');
    }

    public function getdkontes(){
        $getdkontes=Kontes::select('kontes.*');
        return \DataTables::eloquent($getdkontes)
        ->addColumn('nama_tahapank',function($s){
            return '<td>'.$s->belongsto_tahapan->nama_tahapan.'</td>';  
        })
        ->addColumn('jenis_kontes',function($s){
            if($s->jenis_kontes=='1'){
                return '<a href="/admin/update-jenisKontes/'.$s->id.'" class="btn btn-primary"> LKTI</a>';                        
            }    
            else{
                return '<a href="/admin/update-jenisKontes/'.$s->id.'" class="btn btn-secondary"> Olimpiade</a>';
            }     
        })
        ->addColumn('status_kontes',function($s){
                if($s->status_kontes=='1'){
                    return '<a href="/admin/update-statusKontes/'.$s->id.'" class="btn btn-primary"> Aktiv</a>';                        
                }    
                else{
                    return '<a href="/admin/update-statusKontes/'.$s->id.'" class="btn btn-secondary"> Inactive</a>';
                }     
        })
        ->addColumn('aksi',function($s){
            return '<a target="_blank" href="/admin/edit-kontes/'.$s->id.'" class="btn btn-primary"> Edit</a> | 
            <a href="/admin/delete-kontes/'.$s->id.'" class="btn btn-danger"> delete</a>';
        })
        /*->addColumn('link_sertifikat',function($kat){
            $url = asset('storage/images/'.$kat->link_sertifikat); //terimpan di public storage images
            $gambar = '<img src="'.$url.'" style="width: 200px;">';
            return $gambar;
        })*/
        ->rawColumns(['nama_tahapank','jenis_kontes','status_kontes','aksi'])
        ->toJson();
    }
}
