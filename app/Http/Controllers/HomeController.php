<?php

namespace App\Http\Controllers;

use App\Kontes;
use App\Peserta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;


class HomeController extends Controller
{

    public function landing_page()
    {
        $kontes = Kontes::all();
        return view('User.landing-page')->with(['kontes'=>$kontes]);
    }

    public function registrasi(Request $req)
    {
        if ($req->isMethod('post')) {
            $validator = $this->validatorParticipant($req->all());
                
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($req->all());
            }

            $email              = $req->input('email');
            $password           = $req->input('password');
            $confirm_password   = $req->input('confirm_password');
    
            if ($password == $confirm_password) {
                $confirmation_code = str_random(30);
                $peserta            = new Peserta();
                $peserta->email     = $email;
                $peserta->password  = Hash::make($confirm_password);
                $peserta->confirmation_code = $confirmation_code;
                $peserta->email_verification = 0;
                $peserta->save();

                 Mail::send('User.email.email-verification', ['user' => $req['email'], 'confirmation_code' => $confirmation_code, 'type' => 'peserta'], function ($mail) use ($req) {
                     $mail->from('admin@campuspedia.id', 'Campuspedia Team');
                     $mail->to($req['email'], 'Calon Peserta Olimpiade Campuspedia');
                     $mail->subject('Verifikasi alamat email anda.');
                 });

                return redirect('/login')->with('toast_info', 'Silakan cek email untuk memverifikasi akun anda.');
            }
        }else if ($req->isMethod('get')) {
            return view('User.registrasi');
        } else {
            return response()->json(['message'=>'Telah terjadi kesalahan!'], 400);
        }
    }

    public function confirm($type, $confirmation_code)
    {
        // if (!$confirmation_code) {
        //     throw new InvalidConfirmationCodeException;
        // }

        $user = Peserta::where('confirmation_code', $confirmation_code)->first();

        if (!$user) {
            return redirect('/login')->with('toast_info', 'Email sudah terverifikasi, silakan Login');
        }
        $token = str_random(6);

        $user->remember_token = $token;
        $user->email_verification = 1;
        $user->confirmation_code = null;

        if ($user->save()) {

            return redirect('/login')->with('toast_success', 'Email berhasil diverifikasi, silakan Login');
        } else {
            $token = str_random(6);
            $user->remember_token = $token;
            $user->email_verification = 1;
            $user->confirmation_code = null;
            if ($user->save()) {

                return redirect('/login')->with('toast_success', 'Email berhasil diverifikasi, silakan Login');
            } else {
                $token = str_random(6);
                $user->remember_token = $token;
                $user->email_verification = 1;
                $user->confirmation_code = null;
                if ($user->save()) {
                    return redirect('/login')->with('toast_success', 'Email berhasil diverifikasi, silakan Login');
                } else {
                    $msg = "Maaf sistem ada gangguan, silakan <a href='" . URL::to('/resend/' . $type . '/' . $user->email) . "'><strong><u>Kirim Ulang Email</u></strong></a>";
                    return redirect('/login')->with('toast_error', $msg);
                }
            }
        }
    }

    public function reSendEmail($type, $email)
    {

        $user = Peserta::where('email', $email)->first();

        if ($user->email_verification == 0) {
            $selisih = date_diff(date_create(), $user->updated_at);
            if ($selisih->i < 5) {
                $msg = "Email verifikasi telah dikirim ulang kurang dari 5 menit, silakan tunggu minimal 5 menit untuk mengirim ulang. <a href='" . URL::to('/resend/' . $type . '/' . $email) . "'><strong><u>Kirim Ulang Email</u></strong></a>";
                return redirect('/login')->with('toast_warning', $msg);
            }

            Mail::send('User.email.email_verification', ['user' => $user->email, 'confirmation_code' => $user->confirmation_code, 'type' => $type], function ($m) use ($user) {
                $m->from('admin@campuspedia.id', 'Campuspedia Team');
                $m->to($user->email, 'Calon Peserta Olimpiade Campuspedia')->subject('Verifikasi alamat email anda.');
            });

            Peserta::where('email', $email)->update(['updated_at' => date_create()]);

            $msg = "Email verifikasi telah dikirim ulang, silakan tunggu 5 menit dan cek inbox atau folder spam email Anda. <a href='" . URL::to('/resend/' . $type . '/' . $email) . "'><strong><u>Kirim Ulang Email</u></strong></a>";
            return redirect('/login')->with('toast_info', $msg);
        } else {
            return redirect('/login')->with('toast_info', 'Email anda sudah teraktivasi, silakan Login');
        }
    }

    public function login(Request $req)
    {
        if ($req->isMethod('post')) {
            $this->validate($req, [
                'email' => 'required|email|max:255',
                'password' => 'required|max:255'
            ]);

            $email       = $req->input('email');
            $credentials = $req->only('email', 'password');
   
            if (Auth::guard('peserta')->attempt($credentials)) {
                $peserta = Peserta::where('email', $email)->first();
                if ($peserta->email_verification == 1 ) {
                    $peserta->active = $peserta->active + 1;
                    $peserta->save();
                    $req->session()->put('email',  $peserta->email);
                    $req->session()->put('id_peserta',  $peserta->id);
    
                    Alert::success('Anda berhasil login');
        
                    return redirect('/dashboard')->with(['peserta'=>$peserta]);
                } else {
                    return redirect('/login')->with('toast_error', 'Akun anda belum terverifikasi');
                }
            } else {
                $peserta = Peserta::where('email', $email)->first();
                if ($peserta) {
                    $req->session()->flash('message', 'Terdapat kesalahan input pada form.');

                    return redirect('login');
                } else {
                    $req->session()->flash('message', 'Akun anda belum terdaftar.');

                    return redirect('login');
                }
            }    
        } else {
            if ($req->session()->get('email')) {
                return redirect('dashboard');
            }
            return view('User.login');
        }        
    }

    public function validatorParticipant(array $data)
    {
        $rules = [
            'email' => 'required|email|max:50|unique:pesertas',
            'password' => 'required|min:8|required_with:confirm_password',
            'confirm_password' => 'required|min:8|same:password',
        ];
 
        $messages = [
            'email.required'        => 'Email wajib diisi.',
            'email.email'           => 'Email tidak valid.',
            'email.max'             => 'tidak boleh lebih dari 50 karakter.',
            'email.unique'          => 'Email sudah terdaftar.',
            'password.required'     => 'Password wajib diisi.',
            'password.min'          => 'Password tidak boleh kurang dari 8 karakter.',
            'confirm_password.required' => 'Konfirmasi Password wajib diisi.',
            'confirm_password.min'  => 'Konfirmasi Password tidak boleh kurang dari 8 karakter.',
            'confirm_password.same' => 'Password tidak cocok.',
        ];
   
        Validator::extend('without_spaces', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });

        return Validator::make($data, $rules, $messages);
    }
}
