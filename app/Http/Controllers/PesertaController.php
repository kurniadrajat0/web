<?php

namespace App\Http\Controllers;

use App\Kontes;
use App\OlimDataTeam;
use App\OlimPurchase;
use App\Peserta;
use Barryvdh\DomPDF\Facade as PDF;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;


class PesertaController extends Controller
{
    public function dashboard(){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta    = Peserta::where('id', $peserta_id)->first();
        $kontes     = Kontes::all();

        return view('User.dashboard')->with(['all_kontes' => $kontes, 'peserta'=>$peserta]);
    }

    public function lihat_kontes(Request $req){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta    = Peserta::where('id', $peserta_id)->first();

        if ($req->isMethod('post')) {
            $kontes_id  = $req->input('kontes');
            $kontes     = Kontes::with('belongsto_tahapan')->where('id',$kontes_id)->get()->first();

            return view('User.pilih-kontes', ['kontes_dipilih' => $kontes, 'daftar_kontes'=>Kontes::all(), 'peserta'=>$peserta]);
        }
    }

    public function view_kontak(){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta    = Peserta::where('id', $peserta_id)->first();

        return view('User.kontak')->with(['peserta'=>$peserta]);
    }

    public function view_tata_cara_pendaftaran(){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta    = Peserta::where('id', $peserta_id)->first();

        return view('User.tata-cara-pendaftaran')->with(['peserta'=>$peserta]);
    }

    public function view_pengumuman(){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta    = Peserta::where('id', $peserta_id)->first();

        return view('User.pengumuman')->with(['peserta'=>$peserta]);
    }

    public function send_email(){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $purchase    = OlimPurchase::where('peserta_id', $peserta_id)->first();

        return view('User.email.email-tagihan-pembayaran')->with(['purchase'=>$purchase]);
    }

    public function update_profile(Request $req){
        if ($req->isMethod('post')) {
            $filename   = '';
            $gambar     = $req->file('foto_profile');
            $id         = $req->input('id_peserta');
            $peserta    = Peserta::where('id',$id);
            if ($req->hasFile('foto_profile')) {
                $destinationPath    = 'public/image_profile/'.$id;
                $extension          = $gambar->getClientOriginalExtension();
                $filename           = 'profile_'. time() . $id . '.' . $extension;
                $urlFile            = $gambar->storeAs($destinationPath, $filename);
                $peserta->update([
                    'url_photo_profile'     => $urlFile,
                    'email'                 => $req->input('email'),
                    'username'              => $req->input('username'),
                    'nama_lengkap'          => $req->input('nama_lengkap'),
                    'tempat_lahir'          => $req->input('tempat_lahir'),
                    'tanggal_lahir'         => $req->input('tanggal_lahir'),
                    'alamat'                => $req->input('alamat'),
                    'kelurahan'             => $req->input('kelurahan'),
                    'kecamatan'             => $req->input('kecamatan'),
                    'kota'                  => $req->input('kota'),
                    'provinsi'              => $req->input('provinsi'),
                    'jenjang_pendidikan'    => $req->input('jenjang_pendidikan'),
                    'instansi_pendidikan'   => $req->input('instansi_pendidikan'),
                    'no_handphone'          => $req->input('no_handphone'),
                    'whatsapp'              => $req->input('whatsapp'),
                    'line'                  => $req->input('line'),
                    'instagram'             => $req->input('instagram'),
                    'facebook'              => $req->input('facebook'),
                    'twitter'               => $req->input('twitter')
                ]);
            } else {
                $peserta->update([
                    'email'                 => $req->input('email'),
                    'username'              => $req->input('username'),
                    'nama_lengkap'          => $req->input('nama_lengkap'),
                    'tempat_lahir'          => $req->input('tempat_lahir'),
                    'tanggal_lahir'         => $req->input('tanggal_lahir'),
                    'alamat'                => $req->input('alamat'),
                    'kelurahan'             => $req->input('kelurahan'),
                    'kecamatan'             => $req->input('kecamatan'),
                    'kota'                  => $req->input('kota'),
                    'provinsi'              => $req->input('provinsi'),
                    'jenjang_pendidikan'    => $req->input('jenjang_pendidikan'),
                    'instansi_pendidikan'   => $req->input('instansi_pendidikan'),
                    'no_handphone'          => $req->input('no_handphone'),
                    'whatsapp'              => $req->input('whatsapp'),
                    'line'                  => $req->input('line'),
                    'instagram'             => $req->input('instagram'),
                    'facebook'              => $req->input('facebook'),
                    'twitter'               => $req->input('twitter')
                ]);
            }

            if ($peserta) {
                Alert::success('Data berhasil diupdate');
                return redirect()->back();
            }
        } else {
            $peserta_id = Auth::guard('peserta')->user()->id;
            $peserta = Peserta::where('id', $peserta_id)->first();
            $date = \Carbon\Carbon::parse($peserta->tanggal_lahir)->format('Y-m-d');
            return view('User.profile-user')->with(['peserta'=>$peserta, 'tanggal_lahir'=>$date]);
        }
    }

    public function kunci_data(){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $olim_data_team = OlimDataTeam::where('peserta_id',$peserta_id)->first();
        $olim_data_team->status_data = 1;
        $olim_data_team->save();

        Alert::success('Data berhasil dikunci');
        return redirect('/status-pendaftaran');
    }

    public function pilih_kontes(Request $req){
        if ($req->isMethod('post')) {
            $kontes_dipilih = $req->input('kontes');
            $kontes         = Kontes::where('nama_kontes', $kontes_dipilih)->first();
            $nama_peserta   = $req->input('nama_lengkap');
            $email          = $req->input('email');
            $no_telepon     = $req->input('no_telepon');

            if ($kontes) {
                $olim_data_team               = new OlimDataTeam();
                $olim_data_team->peserta_id   = Auth::guard('peserta')->user()->id;
                $olim_data_team->kontes_id    = $kontes->id;
                $olim_data_team->save();
    
            } else {
                return redirect()->back();
            }

            //Generate unique number
            for ($cek = 0; $cek <= 1000; $cek++) {
                $uniq = rand(11, 499);
                $uniq_cek = OlimPurchase::where('unique_number', $uniq)->get();

                if (sizeof($uniq_cek) == 0) {
                    break;
                }
            }

            //Simpan data tagihan
            $tagihan                    = new OlimPurchase();
            $tagihan->peserta_id        = Auth::guard('peserta')->user()->id;
            $tagihan->kontes_id         = $kontes->id;
            $tagihan->unique_number     = $uniq;
            $tagihan->no_invoice        = "INV_" . time();
            $tagihan->kode_peserta      = "MTM-".time();
            $tagihan->tanggal_transaksi = date('Y-m-d');
            $total=0;
            if ($kontes->belongsto_tahapan->harga_kontes > 0) {
                $total                  = $uniq + $kontes->belongsto_tahapan->harga_kontes;
                $tagihan->total_tagihan = $total;
            } else { 
                $tagihan->total_tagihan     = 0;
                $tagihan->status_pembayaran = 1;
            }   
            $tagihan->save();
            $peserta_id = Auth::guard('peserta')->user()->id;
            $peserta    = Peserta::where('id', $peserta_id)->first();   

            $data = DB::table('olim_purchases as op')
                ->join('pesertas as p', 'op.peserta_id', '=', 'p.id')
                ->join('kontes as k', 'op.kontes_id', '=', 'k.id')
                ->select('*')
                ->first();

            Mail::send('User.email.email-tagihan-pembayaran', ['data'=>$data], function ($message) {
                $message->from('admin@campuspedia.id', 'Campuspedia Team');
                $message->to(Auth::guard('peserta')->user()->email, Auth::guard('peserta')->user()->nama_lengkap);
                $message->subject('Invoice Peserta - Olimpiade Campuspedia');
            });

            Alert::success('Data berhasil diinput');

            return redirect('/status-pendaftaran')->with(['kontes'=>$kontes, 'detail_pembayaran'=>$tagihan, 'peserta'=>$peserta]);
        } else {
            $kontes = Kontes::all();
            $peserta_id = Auth::guard('peserta')->user()->id;
            $peserta = Peserta::where('id', $peserta_id)->first();
            
            return view('User.pilih-kontes', ['daftar_kontes' => $kontes, 'kontes_dipilih'=>null, 'peserta'=>$peserta]);
        }
    }

    public function konfirmasi_pembayaran(Request $req){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta = Peserta::where('id', $peserta_id)->first();

        if ($req->isMethod('post')) {
            $metode_pembayaran  = $req->input('metode_pembayaran');
            $tanggal_transfer   = $req->input('tanggal_transfer');
            $file_bukti         = $req->file('file_bukti');
            $kode_voucher       = $req->input('kode_voucher');

            //Generate link upload file bukti pembayaran
            if ($req->hasFile('file_bukti')) {
                // ada file yang diupload
                $fileNameWithExt    = $file_bukti->getClientOriginalName();
                $fileExtension      = $file_bukti->getClientOriginalExtension();
                $fileNameStore      = time() . "_" . $peserta_id . '_file_bukti' . '.' . $fileExtension;
                $url_file           = $file_bukti->storeAs('public/file_bukti_pembayaran', $fileNameStore);

            } else {
                // tidak ada file yang diupload
                $url_file = "noimage.jpg";
            }

            $olim_purchase = OlimPurchase::where('peserta_id', $peserta_id)->get()->first();
            $olim_purchase->metode_pembayaran   = $metode_pembayaran;
            $olim_purchase->tanggal_transfer    = $tanggal_transfer;
            $olim_purchase->file_bukti          = $url_file;
            $olim_purchase->kode_voucher        = $kode_voucher;
            $olim_purchase->status_pembayaran   = -1; //Nanti diedit disisi admin
            $olim_purchase->save();
            
            $data_tim = OlimDataTeam::where('peserta_id', $peserta_id)->get()->first();
            $data_tim -> status_pembayaran = -1;
            $data_tim -> save();
            
            Alert::success('Data berhasil diinput');

            return redirect('/status-pendaftaran')->with(['detail_pembayaran'=>$olim_purchase, 'peserta'=>$peserta]);
        } else {
            return view('User.konfirmasi-pembayaran')->with(['peserta'=>$peserta]);
        }
    }

    public function status_pendaftaran(){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta    = Peserta::where('id', $peserta_id)->first();
        $data_tim = OlimDataTeam::where('peserta_id', $peserta_id)->get()->first();

        if ($data_tim) {
            $kontes_id = $data_tim->kontes_id;
            $purchase = OlimPurchase::where('peserta_id', $peserta_id)->get()->first();
            $kontes = Kontes::where('id', $kontes_id)->get()->first();
            $data = json_decode($data_tim->data_tim,true);
    
            return view('User.status-pendaftaran')->with(['kontes' => $kontes, 'detail_pembayaran' => $purchase, 'data_tim'=>$data_tim, 'peserta'=>$peserta, 'data'=>$data]);
        } else {
            return view('User.status-pendaftaran')->with(['kontes' => null, 'detail_pembayaran' => null, 'data_tim'=>null, 'peserta'=>$peserta]);
        }
    }

    public function daftarkan_tim(Request $req){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta = Peserta::where('id', $peserta_id)->first();
        
        if ($req->isMethod('post')) {
            $input = $req->input();
            $kontes_id = $input['kontes_id'];
            if (!$p = OlimDataTeam::where('kontes_id', $kontes_id)->where('peserta_id', $peserta_id)->first()) {
                $p              = new OlimDataTeam();
                $p->peserta_id  = $peserta_id;
                $p->kontes_id   = $kontes_id;
            } else {
                $input_old = json_decode($p->data_team, true);
            }

            unset($input['_token']);
            unset($input['kontes_id']);
            foreach ($input['nama_peserta'] as $key => $list) {
                if ($req->file('foto_peserta') && isset($req->file('foto_peserta')[$key])) {
                    $destinationPath    = 'public/foto_data_tim/'.$peserta_id;
                    $extension          = $req->file('foto_peserta')[$key]->clientExtension();
                    $nama_file_p        = env("PARTNER_ID", "0")."_foto_".time() . "_" . $peserta_id . $kontes_id . $key . '_file_foto_peserta' . '.' . $extension;
                    $urlFile            = $req->file('foto_peserta')[$key]->storeAs($destinationPath, $nama_file_p);
                    $input['foto_peserta'][$key] = $urlFile;
                } elseif (isset($input_old) && isset($input_old['foto_peserta'][$key])) {
                    $input['foto_peserta'][$key] = $input_old['foto_peserta'][$key];
                }
            }

            $p->data_tim=json_encode($input);
            // $p->status_data=$input['lock_data'];
            $p->save();

            Alert::success('Data tim berhasil disimpan');

            return redirect('/status-pendaftaran')->with(['peserta'=>$peserta]);
        } else {
            if (!isset($_GET['kontes_id'])) {
                $data_tim = OlimDataTeam::where('peserta_id', $peserta_id)->first();

                if($data_tim->status_pembayaran == 0 || $data_tim->status_pembayaran == -1 ){
                    Alert::warning('Silakan lakukan konfirmasi pembayaran');
                    return redirect()->back();
                }
                return view('User.pendaftaran-tim')->with(['data_tim' => $data_tim, 'peserta'=>$peserta]);
            }

            $kontes_id = $_GET['kontes_id'];
            if ($p = OlimDataTeam::where('kontes_id', $kontes_id)->where('peserta_id', $peserta_id)->first()) {
                $data = json_decode($p->data_tim,true);
                return view('User.update-pendaftaran-tim',['kontes_id'=>$kontes_id,'data'=>$data,'lock_data'=>$p->status_data,  'peserta'=>$peserta]);
            } else{
                return view('User.pendaftaran-tim')->with(['kontes_id' => $kontes_id, 'peserta'=>$peserta]);
            }
        }
    }

    public function cetak_kartu(Request $req)
    {
        if ($req->isMethod('post')) {
            $kontes_id = $req->input('kontes_id');
            if ($p = OlimDataTeam::with('get_purchase')->where('kontes_id', $kontes_id)->where('peserta_id', Auth::guard('peserta')->user()->id)->first()) {
                $data = json_decode($p->data_tim,true);
                $purchase = $p->join('olim_purchases', 'olim_purchases.peserta_id', '=', 'olim_data_teams.peserta_id')->select('*')->first();
                $pdf = PDF::loadView('User.template-kartu-peserta', compact('data','purchase'))->setPaper('a4', 'portrait');
                // ini_set('max_execution_time', '300');
                return $pdf->stream('kartu_peserta.pdf');
            }else{  
                Alert::error('Kontes tidak ditemukan');
                return redirect('/status-pendaftaran');
            }
        } else {
            if (!isset($_GET['kontes_id'])) {
                Alert::error('Kontes tidak ditemukan');
                return redirect('/status-pendaftaran');
            }
            $kontes_id = $_GET['kontes_id'];
            if ($p = OlimDataTeam::with('get_purchase')->where('kontes_id', $kontes_id)->where('peserta_id', Auth::guard('peserta')->user()->id)->first()) {
                $data = json_decode($p->data_tim, true);
                $purchase = $p->join('olim_purchases', 'olim_purchases.peserta_id', '=', 'olim_data_teams.peserta_id')->select('*')->first();
                return view('User.template-kartu-peserta', ['kontes_id  ' => $kontes_id, 'data' => $data, 'purchase'=>$purchase]);
            } else {
                Alert::danger('Kontes tidak ditemukan');
                return redirect('/status-pendaftaran');
            }
        }
    }

    public function view_list_sertifikat(){
        $peserta_id = Auth::guard('peserta')->user()->id;
        $peserta = Peserta::where('id', $peserta_id)->first();

        $data = OlimDataTeam::where('olim_data_teams.peserta_id',$peserta_id)
        ->join('kontes','kontes.id','=','olim_data_teams.kontes_id')
        ->join('tahapan_kontes','tahapan_kontes.id','=','kontes.tahapan_kontesid')
        ->orderBy('olim_data_teams.kontes_id','ASC')->get();

        return view('User.list-sertifikat-peserta')->with(['peserta'=>$peserta, 'data'=>$data]);
    }

    public function download_sertif($id){
        //Read json data
        $get_json = Storage::get('public/partisipan_olimpiade/'.$id.'/data.json');
        $no_sertif = json_decode($get_json);

        $nosertif_user=[];
        foreach($no_sertif->data_partisipan as $val){
            if($id==1){
                if(Auth::guard('peserta')->user()->nama_lengkap==$val->peserta){
                    $nosertif_user[0]=$val->sertif_1;
                    $nosertif_user[1]=$val->sertif_2;
                }
            }else{
                if(Auth::guard('peserta')->user()->email==$val->email){
                    $nosertif_user[0]=$val->sertif_1;
                    $nosertif_user[1]=$val->sertif_2;
                }
            }
        }

        $font = public_path() . '/fonts/';
        define('FPDF_FONTPATH',$font);
        $fpdf=new Fpdf();

        $user=OlimDataTeam::where('peserta_id',Auth::guard('peserta')->user()->id)->first();
        $data=json_decode($user->data_tim);

        foreach($data->nama_peserta as $key => $val){
            $fpdf->AddPage('L','A4');
            $fpdf->Image('storage/images/sertifikat/sertif-0'.($id).'.jpg',0,0,297,210);
            $fpdf->AddFont('Montserrat-Bold','','Montserrat-Bold.php');
            $fpdf->setY(55);
            $fpdf->SetTextColor(146,81,155);
            $fpdf->SetFont('Montserrat-Bold','',25);
            $fpdf->Cell(0,0,strtoupper($val),0,2,'C');//nama peserta
            $fpdf->setY(33);
            $fpdf->SetFont('Montserrat-Bold','',12);
            $fpdf->Cell(0,0,'No.'.$nosertif_user[$key],0,1,'C'); //no sertif
        }

        $contents=$fpdf->Output('I','dokumen.pdf');

       return response($contents)->header('Content-Type', 'application/pdf');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }
}
