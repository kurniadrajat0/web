<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Support\MessageBag;
use App\tahapanKontes;
use App\Kontes;
use DB;
use DataTables;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\Redirect;

class tahapanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $dtahapan=tahapanKontes::all();
       return view('Kontes.data_tahapan',compact('dtahapan'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Kontes.create_tahapan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_tahapan' => 'required',
            'harga_kontes' => 'required',
            'tgl_tahapan' => 'required',
            'link_sertifikat' => 'required|mimes:jpg,png,jpeg|max:2048',
            'status_tahapan' => 'required',
        ]);

        $filenameWithExt = $request->file('link_sertifikat')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('link_sertifikat')->getClientOriginalExtension();
        $filenameSimpan = $filename.'.'.$extension;
        $path = $request->file('link_sertifikat')->storeAs('public/images/sertifikat', $filenameSimpan);

        $input=$request->all();
        $status_tahapan = $input['status_tahapan'];
        $input['status_tahapan'] = $status_tahapan;
        $input['link_sertifikat']=$filenameSimpan;
        tahapanKontes::create($input);

        return redirect('admin/data-tahapan')->with('toast_success', 'Data Berhasil Di Buat!');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $etahapan=tahapanKontes::findorfail($id);//buat manggil seluruh variable jabatan
        return view('Kontes.edit_tahapan',compact('etahapan')); #ceompact berfungsi agar variable $peg bisa di pakai di halaman Edit-pegawais
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $etahapan=tahapanKontes::findorfail($id);//Model::findorfail //berfungsi untuk mencari data id menggunakan where agar tidak error jika data tidak ada datanya
        $validator = Validator::make($request->all(), [
            'nama_tahapan' => 'required',
            'harga_kontes' => 'required',
            'tgl_tahapan' => 'required',
            'link_sertifikat' => 'required|mimes:jpg,png,jpeg|max:2048',
            'status_tahapan' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $nama =$_POST['nama_tahapan'];

        $etahapan->nama_tahapan = $nama;

        if ($request->file('link_sertifikat')) {

            $filenameWithExt = $request->file('link_sertifikat')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('link_sertifikat')->getClientOriginalExtension();
            $filenameSimpan = $filename.'.'.$extension;
            $path = $request->file('link_sertifikat')->storeAs('public/images/sertifikat', $filenameSimpan);
            $etahapan->link_sertifikat=$filenameSimpan;
        }
        $etahapan->save();

        return redirect('admin/data-tahapan')->with('toast_success', 'Data Berhasil Di Update!');  //pas udah selesai nyimpen kembali ke data-pegawai ,di akhir tambahan sweet alert
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $etahapan=tahapanKontes::findorfail($id);//Model::findorfail //berfungsi untuk mencari data id menggunakan where agar tidak error jika data tidak ada datanya
        $etahapan->delete();
        return back()->with('info', 'Data Berhasil Di delete!');  //pas udah selesai nyimpen kembali ke data-pegawai ,di akhir tambahan sweet alert
    
    }

    public function update_statusTahapan($id){
        $get_status=DB::table('tahapan_kontes')->select('status_tahapan')->where('id','=',$id)->first();
   
        #check status
        if($get_status->status_tahapan=='1'){
            $status_tahapan='0';}
        else{
            $status_tahapan='1';
        }
        #update status
        $values=array('status_tahapan'=>$status_tahapan);
        DB::table('tahapan_kontes')->where('id',$id)->update($values);
        session()->flash('msg','Product status has been updated successfully');
        return redirect('admin/data-tahapan')->with('toast_success', 'Status tahapan Berhasil Di Update!');
    }

    public function getdtahapan(){
        $gettahapan=tahapanKontes::select('tahapan_kontes.*');
        return \DataTables::eloquent($gettahapan)
        ->addColumn('status_tahapann',function($s){
                if($s->status_tahapan=='1'){
                    return '<a href="/admin/update-statusTahapan/'.$s->id.'" class="btn btn-primary"> Aktiv</a>';                        
                }    
                else{
                    return '<a href="/admin/update-statusTahapan/'.$s->id.'" class="btn btn-secondary"> Inactive</a>';
                }     
        })
        ->addColumn('aksi',function($s){
            return '<a target="_blank" href="/admin/edit-tahapan/'.$s->id.'" class="btn btn-primary"> Edit</a> | 
            <a href="/admin/delete-tahapan/'.$s->id.'" class="btn btn-danger"> delete</a>';
        })
        ->addColumn('gambar',function($kat){
            $url = asset('storage/images/sertifikat/'.$kat->link_sertifikat); //terimpan di public storage images
            $gambar = '<img src="'.$url.'" style="width: 200px;">';
            return $gambar;
        })*/
        ->rawColumns(['status_tahapann','aksi'])
        ->toJson();
    }
}