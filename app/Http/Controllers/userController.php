<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PesertaController;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Support\MessageBag;
use App\Kontes;
use App\OlimDataTeam;
use App\OlimPurchase;
use App\Peserta;
use Illuminate\Support\Facades\Validator;
use DB;
use DataTables;
use Redirect;
use Response;
use PDF;
use File;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $epeserta =DB::table('pesertas')->first();
        return view('Peserta.data_peserta',compact('epeserta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request ,$id)
    {
        if ($req->isMethod('post')) {
            $filename   = '';
            $gambar     = $req->file('foto_profile');
            $id         = $req->input('id_peserta');
            $peserta    = Peserta::where('id',$id);
            if ($req->hasFile('foto_profile')) {
                $destinationPath    = 'public/image_profile/'.$id;
                $extension          = $gambar->getClientOriginalExtension();
                $filename           = 'profile_'. time() . $id . '.' . $extension;
                $urlFile            = $gambar->storeAs($destinationPath, $filename);
                $peserta->updateOrCreate(
                    [
                        'id' => $id
                    ],
                    [
                    'url_photo_profile'=> $urlFile,
                    'username' => $req ->username,
                    'email' => $req ->email,
                    'nama_lengkap' => $req ->nama_lengkap,
                    'tempat_lahir' => $req ->tempat_lahir,
                    'tanggal_lahir' => $req ->tanggal_lahir,
                    'jenjang_pendidikan' => $req ->jenjang_pendidikan,
                    'instansi_pendidikan' => $req ->instansi_pendidikan,
                    'no_handphone' => $req ->no_handphone,
                    'whatsapp' => $req ->whatsapp,
                    'instagram' => $req ->instagram,
                ]);
            }
        }
        /*Peserta::updateOrCreate(
            [
             'id' => $id
            ],
            [
             'username' => $request->username,
             'email' => $request->email,
             'nama_lengkap' => $request->nama_lengkap,
             'tempat_lahir' => $request->tempat_lahir,
             'tanggal_lahir' => $request->tanggal_lahir,
             'jenjang_pendidikan' => $request->jenjang_pendidikan,
             'instansi_pendidikan' => $request->instansi_pendidikan,
             'no_handphone' => $request->no_handphone,
             'whatsapp' => $request->whatsapp,
             'instagram' => $request->instagram,
            ]

           );*/
     
        return response()->json([ 'success Update' => true ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $peserta = Peserta::find($id);
	    return response()->json([
	      'data' => $peserta
	    ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peserta = Peserta::find($id);

        $peserta->delete();
    
        return response()->json([
          'message' => 'Data deleted successfully!'
        ]);
    }

    public function getdpeserta(){
        $getdpeserta =Peserta::select('pesertas.*');
        return \DataTables::of($getdpeserta)
        ->addColumn('jenjang_pendidikan',function($s){
            if ($s->jenjang_pendidikan == 0) {
                return '<td> SMP </td>';  
            } 
            else if($s->jenjang_pendidikan == 1) {
                return '<td> SMA/SMK </td>';  
            }  
            else if($s->jenjang_pendidikan == 2) {
                return '<td> S1 </td>';  
            }  
            else {
                return '<td> Tidak ada </td>'; 
            }
        })
        ->addColumn('aksi',function($s){
            //<a href="" id="editCompany" data-toggle="modal" data-target='#practice_modal' data-id="{{ $item->id }}">Edit</a>

            return '<a data-id='.$s->id.' id="editpeserta" data-toggle="modal" data-target="#data-user" class="btn btn-primary"> View/Edit</a>';
            //return '<a href="/admin/edit-user/'.$s->id.'" class="btn btn-primary"> View/Edit</a>';
        })
        ->rawColumns(['jenjang_pendidikan','aksi'])
        ->toJson();
    }
}
