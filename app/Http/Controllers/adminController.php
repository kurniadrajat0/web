<?php

namespace App\Http\Controllers;

use App\Kontes;
use App\OlimDataTeam;
use App\OlimPurchase;
use App\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class adminController extends Controller
{
    use AuthenticatesUsers;

    public function landing_page()
    {
        $nama = "Hallo Admin";
        return view('Dashboard.beranda_admin',compact('nama'));

        #return view('User.landing-page');
    }

    public function registrasi(Request $req)
    {
        if($req->isMethod('post')){
            $validator = $this->validatorParticipant($req->all());
                
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($req->all());
            }

            $email = $req->input('email');
            $password = $req->input('password');
            $confirm_password = $req->input('confirm_password');
            #belum di ubah
            if($password == $confirm_password){
                $admin = new Admin();
                $admin->email = $email;
                $admin->password = Hash::make($confirm_password);
                $admin->save();
                return redirect('/admin/login')->with('toast_success', 'Akun berhasil terdaftar.');
            }
        }
        else if($req->isMethod('get')){
            return view("Dashboard.registrasi_admin");
            #return view("User.registrasi");
        }
        else{
            return response()->json(['message'=>'Telah terjadi kesalahan!'], 400);
        }
    }

    public function login(Request $req)
    {
        if($req->isMethod('post')){
            $this->validate($req, [
                'email' => 'required|email|max:255',
                'password' => 'required|max:255'
            ]);

            $email = $req->input('email');
            $password = $req->input('password');
    
            $credentials = $req->only('email', 'password');
    
            if (Auth::guard('admin')->attempt($credentials)) {
                $admin = Admin::where('email', $email)->first();
                $req->session()->put('email',  $admin->email);
                $req->session()->put('id_admin',  $admin->id);
    
                Alert::success('Anda berhasil login');
    
                return redirect('/admin/beranda')->with('status', [
                    'enabled' => true,
                    'type' => 'success',
                    'content' => 'Anda berhasil Login',
                ]);
            }
            else {
                $admin = Admin::where('email', $email)->first();
                if ($admin) {
                    $req->session()->flash('message', 'Terdapat kesalahan input pada form.');
                    return redirect('/admin/login');
                } else {
                    $req->session()->flash('message', 'Akun anda belum terdaftar.');
                    return redirect('/admin/login');
                }
            }    
        }
        else{
            if($req->session()->get('email')){
                return redirect('/admin/beranda');
            }
            return view('Dashboard.login_admin');
        }
    }

    public function dashboard(){
        $kontes = Kontes::all();
        return view('Dashboard.beranda_admin', ['all_kontes' => $kontes]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/admin/login');
    }

     public function validatorParticipant(array $data)
    {
        $rules = [
            'email' => 'required|email|max:50|unique:admins',
            'password' => 'required|min:8|required_with:confirm_password',
            'confirm_password' => 'required|min:8|same:password',
        ];
 
        $messages = [
            'email.required'        => 'Email wajib diisi.',
            'email.email'           => 'Email tidak valid.',
            'email.max'             => 'tidak boleh lebih dari 50 karakter.',
            'email.unique'          => 'Email sudah terdaftar.',
            'password.required'     => 'Password wajib diisi.',
            'password.min'          => 'Password tidak boleh kurang dari 8 karakter.',
            'confirm_password.required' => 'Konfirmasi Password wajib diisi.',
            'confirm_password.min'  => 'Konfirmasi Password tidak boleh kurang dari 8 karakter.',
            'confirm_password.same' => 'Password tidak cocok.',
        ];

        Validator::extend('without_spaces', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });

        return Validator::make($data, $rules, $messages);
    }
}
