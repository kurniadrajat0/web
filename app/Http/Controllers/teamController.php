<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Controllers\PesertaController;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Support\MessageBag;
use App\Kontes;
use App\OlimDataTeam;
use App\OlimPurchase;
use App\Peserta;
use Illuminate\Support\Facades\Validator;
use DB;
use DataTables;
use Illuminate\Support\Facades\DB as FacadesDB;
use Redirect;

class teamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $duser=Peserta::latest()->paginate(10);
        $data_tim = DB::table('pesertas')
                ->join('olim_data_teams', 'olim_data_teams.peserta_id', '=', 'pesertas.id')
                ->get();
        
        if ($data_tim) {
            $data = json_decode($data_tim[0]->data_tim, true);
            return view('Team.data_team',compact('data_tim','data','duser'));
        }
        return view('Team.data_team',compact('data_tim','duser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getdteam(){
        $getdpeserta =Peserta::select('pesertas.*');
        return \DataTables::of($getdpeserta)
        ->addColumn('jenjang_pendidikan',function($s){
            if ($s->jenjang_pendidikan == 0) {
                return '<td> SMP </td>';  
            } 
            else if($s->jenjang_pendidikan == 1) {
                return '<td> SMA/SMK </td>';  
            }  
            else if($s->jenjang_pendidikan == 2) {
                return '<td> S1 </td>';  
            }  
            else {
                return '<td> Tidak ada </td>'; 
            }
        })
        ->addColumn('aksi',function($s){
            return '<a data-id='.$s->id.' data-toggle="modal" data-target="#data-team" class="btn btn-primary"> View/Edit</a>';
        })
        ->rawColumns(['jenjang_pendidikan','aksi'])
        ->toJson();
    
    }
}
