<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class tahapanKontes extends Model
{
    protected $table ="tahapan_kontes";
    protected $primaryKey ="id";
    protected $fillable=[
        'id', 'nama_tahapan', 'harga_kontes','tgl_tahapan',
        'link_sertifikat','status_tahapan'
    ];
    //buat one to many
    public function hasmany_kontes()
    {
        return $this->hasMany(Kontes::class); //di model tahapan nulis kontes,bgtu juga sebaliknya
    }

}
