<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('Gambar/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Page</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('Gambar/laravel.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Admin Campuspedia</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{route('data-user')}}" class="nav-link active">
              <i class="far fa-user"></i>
              <p>User</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('data-team')}}" class="nav-link active">
              <i class="fa fa-users"></i>
              <p>Team</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="fa fa-credit-card"></i>
              <p>Voucher</p>
            </a>
          </li>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="fa fa-cart-plus"></i>
              <p>Transaksi
              <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('transaksi-baru')}}" class="nav-link active">
                  <i class="fa fa-plus-circle"></i>
                  <p>Data Transaksi Baru</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('transaksi-disetujui')}}" class="nav-link active">
                  <i class="fa fa-check-circle"></i>
                  <p>Data Transaksi Disetujui</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('transaksi-ditolak')}}" class="nav-link active">
                  <i class="fa fa-ban"></i>
                  <p>Data Transaksi Ditolak</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Kontes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('data-kontes')}}" class="nav-link active">
                  <i class="fa fa-trophy"></i>
                  <p>Data Kontes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('data-tahapan')}}" class="nav-link active">
                  <i class="fa fa-list-ol"></i>
                  <p>Tahapan Kontes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inactive Page</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="/admin/logout" class="nav-link">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>