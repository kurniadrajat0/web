
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en"> 
<head>
  @include('Template.header')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Starter Page</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('beranda-admin')}}">Home</a></li>
              <li class="breadcrumb-item active">Data Transaksi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card card-info card-outline">
            <div class="card-header">
              <h3> Update Data Transaksi</h3>
            </div>
            <div class="card-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
              @endif
              <form action="{{url('admin/update-transaksi',$epurchase->id)}}" method="post" enctype="multipart/form-data"><!--Ubah ini -->
                @csrf
                <div class="form-group">
                    <label>Nama Peserta</label>
                    <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" value="{{ $epurchase->hasOne_peserta->nama_lengkap }}" disabled>
                </div>
                <div class="form-group">
                    <label>Nama Kontes</label>
                    <input type="text" class="form-control" placeholder="Nama Kontes" id="kontes_id" name="kontes_id" value="{{ $epurchase->kontes_id }}" disabled>
                </div>
                <div class="form-group">
                    <label>Tanggal Transaksi</label>                   
                    <input type="date" id="tanggal_transaksi" name="tanggal_transaksi" class="form-control" value="{{$epurchase->tanggal_transaksi}}" disabled>
                </div>
                <div class="form-group">
                  <label>Status Pembayaran</label>
                  <select class="form-control" name="status_pembayaran" value="{{$epurchase->status_pembayaran}}"><!-- tambahin multiple="" kalo error-->
                    <option value="-1">Pending/Baru</option>
                    <option value="0">Ditolak/Belum lunas</option>
                    <option value="1">Disetujui/Lunas</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Total Tagihan</label>
                  <input type="text" class="form-control" placeholder="Total Tagihan" id="total_tagihan" name="total_tagihan" value="{{ $epurchase->total_tagihan }}" disabled>
                </div>
                <div class="form-group">
                  <label>Metode Pembayaran</label>
                  <select class="form-control" id="metode_pembayaran" name="metode_pembayaran" value="{{$epurchase->metode_pembayaran}}" disabled>
                    <option value="Metode A">Metode A</option>
                    <option value="Metode B">Metode B</option>
                    <option value="Metode C">Metode C</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="form-label" for="customFile">File Bukti</label>
                  <input type="file" class="form-control" id="file_bukti" name="file_bukti" placeholder="File Bukti" disabled/>{{$epurchase->file_bukti}} 
                </div>
                <div class="form-group">
                  <img src="{{Storage::url($epurchase->file_bukti)}}" alt="">
                </div> 
                
                <div class="form-group">
                  <button type="submit" class="btn btn-primary">Update Data</button>
                </div>
              </form>
            </div>
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    @include('Template.footer')
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

@include('Template.script')
</body>
</html>
