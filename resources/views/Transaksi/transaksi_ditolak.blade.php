
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en"> 
<head>
  @include('Template.header')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Transaksi Di Tolak</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('beranda-admin')}}">Home</a></li>
              <li class="breadcrumb-item active">Data Transaksi Di Tolak</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card card-info card-outline">
            <div class="card-header">
                <div class="card-tools">
                    <a href="{{route('create-kontes')}}" class="btn btn-success">Tambah data <i class="fas fa-plus-square"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="datatable">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th> Nama Peserta </th>
                      <th> Jenis Kontes </th>
                      <th> Tanggal Transaksi </th>       
                      <th> Status Pembayaran </th>
                      <th> Total Tagihan</th>
                      <th> Metode Pembayaran </th>
                      <th> Aksi </th>
                    </tr>       
                  </thead>
                  <tbody>

                  </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    @include('Template.footer')
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

@include('Template.script')
@include('sweetalert::alert')
<script>
  $(document).ready(function() {
    $('#datatable').DataTable({
      processing:true,
      serverside:true,
      ajax:"{{route('ajax.getdtransaksi_ditolak')}}",
      columns:[
        {data:'id',name:'id'},
        {data:'nama_peserta',name:'nama_peserta'},
        {data:'jenis_kontes',name:'jenis_kontes'},
        {data:'tanggal_transaksi',name:'tanggal_transaksi'},
        {data:'status_pembayaran',name:'status_pembayaran'},
        {data:'total_tagihan',name:'total_tagihan'},
        {data:'metode_pembayaran',name:'metode_pembayaran'},
        {data:'aksi',name:'aksi'},
      ]
    });
  });
</script>
</body>
</html>
