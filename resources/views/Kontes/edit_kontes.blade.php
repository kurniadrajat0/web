
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en"> 
<head>
  @include('Template.header')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Starter Page</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('beranda-admin')}}">Home</a></li>
              <li class="breadcrumb-item active">Data Kontes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card card-info card-outline">
            <div class="card-header">
              <h3> Update Data Kontes</h3>
            </div>
            <div class="card-body">
              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              <form action="{{url('admin/update-kontes',$ekontes->id)}}" method="post" enctype="multipart/form-data"><!--Ubah ini -->
                @csrf
                <div class="form-group">
                    <label>Nama Kontes</label>
                    <input type="text" class="form-control" placeholder="Nama Kontes" id="nama_kontes" name="nama_kontes" value="{{ $ekontes->nama_kontes }}">
                </div>
                <div class="form-group">
                  <label><strong> Jenis Kontes</strong></label><br/>
                  <select class="form-control" name="jenis_kontes" value="{{$ekontes->jenis_kontes}}"><!-- tambahin multiple=""-->
                    <option value="1">LKTI</option>
                    <option value="0">Olimpiade</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Deskripsi Kontes</label>
                  <input type="text" class="form-control" placeholder="Deskripsi Kontes" id="deskripsi_kontes" name="deskripsi_kontes" value="{{ $ekontes->deskripsi_kontes }}">
                </div>
                <div class="form-group">
                  <label>Tahapan Kontes</label>
                  <select class="form-control" name="tahapan_kontesid" id="tahapan_kontesid"><!--daimbil dari model kontes -->
                    <option disabled value>Tahapan Kontes</option>
                    <option value="{{ $ekontes->tahapan_kontesid }}">{{$ekontes->belongsto_tahapan->nama_tahapan}}</option>
                    @foreach ($etahap as $i)<!--tahap dari controller create -->
                      <option value="{{$i->id}}">{{$i->nama_tahapan}}</option> <!--ambil id dan nama jabatan dari fillable -->
                    @endforeach
                  </select>
                </div>              
                <div class="form-group">
                  <label>Tanggal Kontes</label>
                  <input type="date" id="tgl_kontes" name="tgl_kontes" class="form-control" value="{{ $ekontes->tgl_kontes }}">
                </div>    
                <div class="form-group">
                  <label>Harga Kontes</label>
                  <input type="text" class="form-control" placeholder="Harga Kontes" id="harga_kontes" name="harga_kontes" value="{{ $ekontes->harga_kontes }}">
                </div>
                <div class="form-group">
                  <label class="form-label" for="customFile">Link Sertifikat</label>
                  <input type="file" class="form-control" id="link_sertifikat" name="link_sertifikat" placeholder="Link Sertifikat" />{{ $ekontes->link_sertifikat }}
                </div>
                <div class="form-group">
                  <img src="{{asset('storage/images/'.$ekontes->link_sertifikat)}}" alt="">
                </div>
                <div class="form-group">
                  <label>Status Kontes</label>
                  <select class="form-control" name="status_kontes" value="{{$ekontes->status_kontes}}" ><!-- tambahin multiple="" kalo error-->
                    <option value="1">Aktiv</option>
                    <option value="0">Tidak Aktiv</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Pengumuman Kontes</label>
                  <input type="text" class="form-control" placeholder="Pengumuman Kontes" id="pengumuman_kontes" name="pengumuman_kontes" value="{{$ekontes->pengumuman_kontes}}">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary"> Update Data</button>
                </div>
              </form>
            </div>
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    @include('Template.footer')
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

@include('Template.script')
</body>
</html>
