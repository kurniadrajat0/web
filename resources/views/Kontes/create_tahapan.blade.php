
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en"> 
<head>
  @include('Template.header')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Starter Page</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('beranda-admin')}}">Home</a></li>
              <li class="breadcrumb-item active">Tahapan Kontes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card card-info card-outline">
            <div class="card-header">
              <h3> Create Tahapan Kontes</h3>
            </div>
            <div class="card-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
              @endif
              <form action="{{route('simpan-tahapan')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Nama Tahapan</label>
                    <input type="text" class="form-control" placeholder="Nama Tahapan Kontes" id="nama_tahapan" name="nama_tahapan">
                    <span class="text-danger">{{ $errors->first('nama_tahapan') }}</span>
                </div>
                <div class="form-group">
                  <label>Harga Kontes</label>
                  <input type="text" id="harga_kontes" name="harga_kontes" class="form-control" placeholder="Harga Kontes">
                  <span class="text-danger">{{ $errors->first('harga_kontes') }}</span>
                </div>
                <div class="form-group">
                  <label>Tanggal Tahapan Kontes</label>
                  <input type="date" id="tgl_tahapan" name="tgl_tahapan" class="form-control">
                  <span class="text-danger">{{ $errors->first('tgl_tahapan') }}</span>
                </div>
                <div class="form-group">
                  <label class="form-label" for="customFile">Gambar Sertifikat</label>
                  <input type="file" class="form-control" id="link_sertifikat" name="link_sertifikat" placeholder="Link Sertifikat"/>
                  <span class="text-danger">{{ $errors->first('link_sertifikat') }}</span>
                </div>
                <div class="form-group">
                  <label>Status Kontes</label>
                  <select class="form-control" name="status_tahapan" ><!-- tambahin multiple="" kalo error-->
                    <option value="1">Aktiv</option>
                    <option value="0">Tidak Aktiv</option>
                  </select>
                  <span class="text-danger">{{ $errors->first('status_tahapan') }}</span>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-success"> Simpan Data</button>
                </div>
              </form>
            </div>
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    @include('Template.footer')
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

@include('Template.script')
</body>
</html>
