<!-- Modal -->
  <div class="modal fade" id="registrasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Registrasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('user.pilih-kontes') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="exampleFormControlSelect1">Pilih kontes</label>
                    @if ($kontes_dipilih != null)
                    <input type="text" class="form-control form-control-user" name="kontes" value="{{ $kontes_dipilih->nama_kontes }}" readonly>
                    @endif
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Nama Lengkap</label>
                <input type="text" class="form-control form-control-user" name="nama_lengkap" id="nama_lengkap" value="{{$peserta->nama_lengkap}}">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Email</label>
                <input type="text" class="form-control form-control-user" name="email" id="email" value="{{$peserta->email}}">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">No. Telepon</label>
                <input type="text" class="form-control form-control-user" name="no_telepon" id="no_telepon" value="{{$peserta->no_handphone}}">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary btn-user">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>
