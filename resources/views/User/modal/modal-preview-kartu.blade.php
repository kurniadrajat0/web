<!-- Modal -->
@if ($kontes != null)
<div class="modal fade" id="kartu-peserta" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cetak Kartu Peserta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    
      <div class="modal-footer">
        <a class="btn btn-secondary" data-dismiss="modal">Kembali</a>
        <form class="form" action="{{URL::to('/cetak_kartu')}}" method="post" enctype="multipart/form-data">
          @csrf
          <input hidden name="kontes_id" value="{{$kontes->id}}">
          <button type="submit" class="btn btn-primary">Print</button>
        </form> 
        <a href="{{URL::to('/cetak_kartu/?kontes_id='.$kontes->id)}}" class="btn btn-primary" target="_blank">Lihat</a>
      </div>
    </div>
  </div>
</div>
@endif
