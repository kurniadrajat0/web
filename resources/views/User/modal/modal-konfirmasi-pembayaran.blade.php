@if ($detail_pembayaran != null && ($detail_pembayaran->status_pembayaran == 1 || $detail_pembayaran->status_pembayaran == -1))
    <!-- Modal -->
    <div class="modal fade" id="konfirmasi_pembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Informasi Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td><strong>No. Invoice</strong></td>
                            <td>:</td>
                            <td><p class="float-right mb-0">{{ $detail_pembayaran->no_invoice }}</p></td>
                        </tr>
                        <tr>
                            <td><strong>Metode Pembayaran</strong></td>
                            <td>:</td>
                            <td><p class="float-right mb-0">{{ $detail_pembayaran->metode_pembayaran }}</p></td>
                        </tr>
                        <tr>
                            <td><strong>Tanggal Transfer</strong></td>
                            <td>:</td>
                            <td><p class="float-right mb-0">{{date_format(date_create($detail_pembayaran->tanggal_transfer),"d/m/Y")}} </p></td>
                        </tr>   
                        <tr>
                            <td><strong>Bukti Pembayaran</strong></td>
                            <td>:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3"><img src="{{ Storage::url($detail_pembayaran->file_bukti) }}" alt=""></td>
                        </tr>
                        <tr>
                            <td><strong>Potongan harga</strong></td>
                            <td>:</td>
                            @if ($detail_pembayaran->potongan_voucher == null)
                                <td><p class="float-right mb-0"> - </p></td>   
                            @else
                                <td><p class="float-right mb-0">Rp{{ $detail_pembayaran->potongan_voucher }}</p></td>  
                            @endif
                        </tr>
                        <tr>
                            <td><strong>Total tagihan</strong></td>
                            <td>:</td>
                            <td><p class="float-right mb-0">Rp{{ $detail_pembayaran->total_tagihan }}</p></td>
                        </tr>
                        <tr>
                            <td><strong>Tagihan dibayarkan</strong></td>
                            <td>:</td>
                            <td><p class="float-right mb-0">Rp{{ $detail_pembayaran->total_tagihan }}</p></td>
                        </tr>
                        <tr>
                            <td><strong>Status</strong> </td>
                            <td>:</td>
                            @if ($detail_pembayaran->status_pembayaran == -1)
                                <td><span class="uk-label uk-label-warning float-right mb-0"> Pending </span></td>
                            @elseif ($detail_pembayaran->status_pembayaran == 1)
                                <td><span class="uk-label uk-label-success float-right mb-0"> Lunas </span></td>
                            @endif
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-user" data-dismiss="modal">Kembali</button>
                </div>
            </div>
        </div>
    </div>
    @else
    <!-- Modal -->
    <div class="modal fade" id="konfirmasi_pembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Informasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Tagihan anda belum dibayarkan.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-user" data-dismiss="modal">Kembali</button>
                </div>
            </div>
        </div>
    </div>
@endif