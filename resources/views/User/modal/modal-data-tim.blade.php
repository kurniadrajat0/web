<!-- Modal -->
@if ($data_tim != null)
<div class="modal fade bd-example-modal-xl" id="data-tim" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Informasi Data Tim</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @if ($data_tim->data_tim == null)
            <div class="modal-body">
              Data tim belum terdaftar
            </div>
        @else
        <div class="modal-body">     
            <div class="btn-inline float-right">
              <a class="btn btn-outline-primary btn-user @if ($data_tim->status_data == 1) disabled @endif" href="{{URL::to('/kunci-data')}}" style="margin-bottom:10px;margin-left:10px;">Kunci Data</a> 
              <a class="btn btn-primary btn-user @if ($data_tim->status_data == 1) disabled @endif" href="{{URL::to('/pendaftaran-tim/?kontes_id='.$data_tim->kontes_id)}}" style="margin-bottom:10px;" >Ubah Data</a> 
            </div>
            <div class="table-responsive">
              <table class="table">
                <thead class="text-white bg-primary">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Peserta</th>
                    <th scope="col">Tempat Lahir</th>
                    <th scope="col">Tanggal Lahir</th>
                    <th scope="col">No Telepon</th>
                    <th scope="col">Instagram</th>
                    <th scope="col">Unggahan Foto</th>
                </tr>
                </thead>
                <tbody>
                @php($awal=0)
                @foreach($data['nama_peserta'] as $ke=>$list)
                <tr>
                  <th scope="row">{{$loop->iteration}}</th>
                  <td>{{$data['nama_peserta'][$ke]}}</td>
                  <td>{{$data['tempat_lahir'][$ke]}}</td>
                  <td>{{date_format(date_create($data['tgl_lahir'][$ke]),"d-m-Y")}}</td>
                  <td>{{$data['no_telepon'][$ke]}}</td>
                  <td>{{$data['instagram'][$ke]}}</td>
                  <td>  
                    <?php if(isset($data['foto_peserta'][$ke])){ ?>
                    <img width="150" src="{{Storage::url($data['foto_peserta'][$ke])}}" />
                    <?php } ?>
                  </td>
                </tr>
                @php($awal=$ke)
                @endforeach
                </tbody>
            </table>
          </div>
        </div>
        @endif
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btn-user" data-dismiss="modal">Kembali</button>
          <button type="button" class="btn btn-danger btn-user @if ($data_tim->status_data == 1) disabled @endif" style="align-items:left;">Hapus</button>
          </div>
      </div>
    </div>
</div>
@endif