@extends('User-Layouts.master')

@section('title', 'Status Pendaftaran')

@section('extracss')
    <link href="{{asset('css/timeline.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.6.20/dist/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.20/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.20/dist/js/uikit-icons.min.js"></script>
@endsection

@section('extrajs')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>        
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">STATUS PENDAFTARAN</h1>
</div>
@if ($kontes == null || $detail_pembayaran == null)
    <h3 class="h4 text-gray-900 text-center justify-content-center mb-4" style="opacity: 50%;width:100%">
        Anda belum memilih kontes
    </h3>
@else
<div class="uk-container uk-padding">
    <div class="uk-timeline">
        <div class="uk-timeline-item">
            <div class="uk-timeline-icon">
                @if ($kontes == null)
                <span class="uk-badge" style="background: rgb(120, 120, 120)">
                    <span uk-icon= "close"></span>
                </span>
                @else
                <span class="uk-badge">
                    <span uk-icon="check"></span>
                </span>
                @endif
            </div>
            <div class="uk-timeline-content">
                <div class="uk-card uk-card-default uk-margin-medium-bottom uk-overflow-auto">
                    <div class="uk-card-header">
                        <div class="uk-grid-small uk-flex-middle" uk-grid>
                            <h3 class="uk-card-title"><a href="/pilih-kontes" style="color: #333;"> Pilih Kontes </a></h3>
                            <span class="uk-label uk-label-success uk-margin-auto-left">Dipilih</span>
                        </div>
                    </div>
                    <div class="uk-card-body">
						<p class="">Kontes yang diikuti : {{ $kontes->nama_kontes }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-timeline-item">
            <div class="uk-timeline-icon">
                @if ($detail_pembayaran->status_pembayaran == 0)
                    <span class="uk-badge" style="background: rgb(120, 120, 120)">
                        <span uk-icon= "close"></span>
                    </span>
                @else
                    <span class="uk-badge">
                        <span uk-icon="check"></span>
                    </span>
                @endif
            </div>
            <div class="uk-timeline-content">
                <div class="uk-card uk-card-default uk-margin-medium-bottom uk-overflow-auto">
                    <div class="uk-card-header">
                        <div class="uk-grid-small uk-flex-middle" uk-grid>
                            <h3 class="uk-card-title"><a href="{{URL::to('/konfirmasi-pembayaran')}}" style="color: #333;"> Konfirmasi Pembayaran </a></h3>
                            @if ($detail_pembayaran->status_pembayaran == 0)
                                <span class="uk-label uk-label-danger uk-margin-auto-left">Belum Lunas</span>
                            @elseif ($detail_pembayaran->status_pembayaran == -1)
                                <span class="uk-label uk-label-warning uk-margin-auto-left">Pending</span>
                            @else
                                <span class="uk-label uk-label-success uk-margin-auto-left">Lunas</span>
                            @endif
                        </div>
                    </div>
                    <div class="uk-card-body">
                        <a href="" data-toggle="modal" data-target="#konfirmasi_pembayaran">
                            <span class="uk-label uk-label-primary uk-margin-auto-left"> Lihat Selengkapnya</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="uk-timeline-item">
            <div class="uk-timeline-icon">
                @if ($data_tim->data_tim == null)
                <span class="uk-badge" style="background: rgb(120, 120, 120)">
                    <span uk-icon= "close"></span>
                </span>
                @else
                    <span class="uk-badge">
                        <span uk-icon="check"></span>
                    </span>
                @endif
            </div>
            <div class="uk-timeline-content">
                <div class="uk-card uk-card-default uk-margin-medium-bottom uk-overflow-auto">
                    <div class="uk-card-header">
                        <div class="uk-grid-small uk-flex-middle" uk-grid>
                            <h3 class="uk-card-title"><a href="{{URL::to('/pendaftaran-tim')}}" style="color: #333;"> Daftarkan Tim </a></h3>
                             @if ($data_tim->data_tim == null)
                                <span class="uk-label uk-label-danger uk-margin-auto-left">Belum Terdaftar</span>
                            @else
                                <span class="uk-label uk-label-success uk-margin-auto-left">Terdaftar</span>
                                @if ($data_tim->status_data == 0)
                                    <span class="uk-label uk-label-danger ml-2">Belum dikunci</span>
                                @else
                                    <span class="uk-label uk-label-success ml-2">Data dikunci</span>
                                @endif
                            @endif
                                
                        </div>
                    </div>
                    <div class="uk-card-body">
                        <a href="" data-toggle="modal" data-target="#data-tim">
                            <span class="uk-label uk-label-primary uk-margin-auto-left"> Lihat Selengkapnya</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-timeline-item">
            <div class="uk-timeline-icon">
                @if ($data_tim->status_data == 0)  {{-- Jika data belum dikunci --}}
                    <span class="uk-badge" style="background: rgb(120, 120, 120)">
                        <span uk-icon= "close"></span>
                    </span>
                @else  {{-- Jika data sudah dikunci --}}
                    <span class="uk-badge">
                        <span uk-icon="check"></span>
                    </span>
                @endif
            </div>
            <div class="uk-timeline-content">
                <div class="uk-card uk-card-default uk-margin-medium-bottom uk-overflow-auto">
                    <div class="uk-card-header">
                        <div class="uk-grid-small uk-flex-middle" uk-grid>
                            <h3 class="uk-card-title"><a href="" style="color: #333;"> Cetak Kartu </a></h3>
                        </div>
                    </div>
                    <div class="uk-card-body">
                        <a href="" data-toggle="modal" data-target="#kartu-peserta">
                            <span class="uk-label uk-label-primary uk-margin-auto-left">Lihat Selengkapnya</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>


		<div class="uk-timeline-item">
            <div class="uk-timeline-icon">
                <span class="uk-badge" style="background: rgb(120, 120, 120)">
                    <span uk-icon="close">
                    </span>
                </span>
            </div>
            <div class="uk-timeline-content">
                <div class="uk-card uk-card-default uk-margin-medium-bottom uk-overflow-auto">
                    <div class="uk-card-header">
                        <div class="uk-grid-small uk-flex-middle" uk-grid>
                            <h3 class="uk-card-title"><a href={{($kontes->jenis_kontes == 1) ? "/" : URL::to('/file-lkti/?kontes_id='.$kontes->id)}} style="color: #333;"> Ikuti Kontes </a></h3>
                        </div>
                    </div>
                </div>
				{{-- <a href="#"><span class="uk-margin-small-right" uk-icon="triangle-down"></span>Load more</a> --}}
            </div>
        </div>
    </div>
</div>
@endif

@include('User.modal.modal-konfirmasi-pembayaran')
@include('User.modal.modal-data-tim')
@include('User.modal.modal-preview-kartu')
        
@endsection

