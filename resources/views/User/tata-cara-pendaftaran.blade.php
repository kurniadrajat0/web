@extends('User-Layouts.master')

@section('title', 'Tata Cara Pendaftaran')

@section('extracss')
    <style>
        .video-container {
            overflow: hidden;
            /* 16:9 aspect ratio */
            padding-top: 56.25%;
            position: relative;
        }

        .video-container iframe{
            height: 100%;
            left: 0;
            position: absolute;
            top: 0;
        }
    </style>
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">TATA CARA PENDAFTARAN</h1>
</div>

<div class="container-fluid">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl col-lg-8 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-2">    
                    <div class="video-container">
                        <iframe width="100%" src="https://www.youtube.com/embed/Gu2cPPB24HE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                                                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

