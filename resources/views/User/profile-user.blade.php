@extends('User-Layouts.master')

@section('title', 'Profile User')

@section('extracss')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('extrajs')
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>        
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">PROFILE USER</h1>
</div>

<div class="container-fluid">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
                        <div class="col-lg">
                            <div class="p-5">
                                <form class="user" method="POST" action="{{route('user.profile')}}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="text" name="id_peserta" value="{{$peserta->id}}" id="" hidden>
                                    <fieldset class="fieldset">
                                        <h3 class="fieldset-title">Data Akun</h3>
                                        <br>
                                        <div class="form-group row">
                                            <figure class="figure col-md-2 col-sm-3 col-xs-12">
                                              @if ($peserta->url_photo_profile == null)
                                                <img class="img-profile rounded-circle" style="width:150px;height:150px;" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="">
                                              @else
                                                <img class="img-profile rounded-circle" style="width:150px;height:150px;" src="{{ Storage::url($peserta->url_photo_profile) }}"  alt="">                                                 
                                              @endif
                                            </figure>
                                            <div class="form-inline col-md-10 col-sm-9 col-xs-12">
                                                <input type="file" name="foto_profile" class="form-control form-control-user pull-left" style="padding:5px;margin-right:10px;">
                                                <button type="submit" class="btn btn-outline-primary btn-user btn-sm btn-default-alt pull-left">Update Image</button>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                          <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Username</strong></label>
                                          <div class="col-sm-10">
                                            <input type="text" class="form-control form-control-user" name="username" id="username" value="{{$peserta->username}}">
                                          </div>
                                        </div>
                                        <div class="form-group row">
                                          <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Email</strong></label>
                                          <div class="col-sm-10">
                                            <input type="text" class="form-control form-control-user" name="email" id="email" value="{{$peserta->email}}">
                                          </div>
                                        </div>      

                                    </fieldset>
                                    <hr>
                                    <fieldset class="fieldset">
                                        <h3 class="fieldset-title">Biodata Diri</h3>
                                        <br>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Nama Lengkap</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="nama_lengkap" id="nama_lengkap" value="{{$peserta->nama_lengkap}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Tempat Lahir</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="tempat_lahir" id="tempat_lahir" value="{{$peserta->tempat_lahir}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Tanggal Lahir</strong></label>
                                            <div class="col-sm-10">
                                              <input type="date" class="form-control form-control-user" name="tanggal_lahir" id="tanggal_lahir" value="{{$tanggal_lahir}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Alamat Tinggal</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="alamat" id="alamat" value="{{$peserta->alamat}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Kelurahan</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="kelurahan" id="kelurahan" value="{{$peserta->kelurahan}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Kecamatan</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="kecamatan" id="kecamatan" value="{{$peserta->kecamatan}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Kota</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="kota" id="kota" value="{{$peserta->kota}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Provinsi</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="provinsi" id="provinsi" value="{{$peserta->provinsi}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Jenjang Pendidikan</strong></label>
                                            <div class="col-sm-10">
                                                <select name="jenjang_pendidikan" name="jenjang_pendidikan" id="jenjang_pendidikan" class="form-control form-control-user">
                                                  <option @if ($peserta->jenjang_pendidikan == null) selected @endif> -- Pilih jenjang pendidikan -- </option>  
                                                  <option value="0" @if ($peserta->jenjang_pendidikan == 0) selected @endif >SMP</option>
                                                  <option value="1" @if ($peserta->jenjang_pendidikan == 1) selected @endif>SMA/SMK</option>
                                                  <option value="2" @if ($peserta->jenjang_pendidikan == 2) selected @endif>S1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Instansi Pendidikan</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="instansi_pendidikan" id="instansi_pendidikan" value="{{$peserta->instansi_pendidikan}}">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <hr>
                                    <fieldset class="fieldset">
                                        <h3 class="fieldset-title">Contact Info</h3>
                                        <br>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>No. Handphone</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="no_handphone" id="no_handphone" value="{{$peserta->no_handphone}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Whatsapp</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="whatsapp" id="whatsapp" value="{{$peserta->whatsapp}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>LINE</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="line" id="line" value="{{$peserta->line}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Instagram</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="instagram" id="instagram" value="{{$peserta->instagram}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Facebook</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="facebook" id="facebook" value="{{$peserta->facebook}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label"><strong>Twitter</strong></label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control form-control-user" name="twitter" id="twitter" value="{{$peserta->twitter}}">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <br>
                                    <button
                                    type="submit"
                                    class="btn btn-primary btn-user btn-block"
                                    style="font-weight: 800;font-size:16px;"
                                    >
                                        Update Profile
                                    </button>
                                  </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

