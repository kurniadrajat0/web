@extends('User-Layouts.master')

@section('title', 'Pilih Kontes')

@section('extrajs')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">DAFTAR OLIMPIADE</h1>
</div>

<div class="container-fluid">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl col-lg-8 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h3 class="h4 text-gray-900 mb-4">PILIH KONTES</h3>
                                </div>
                                <form class="form-inline float-right" method="post" action="{{ route('user.lihat-kontes') }}">
                                    @csrf
                                    <div class="form-group mx-sm-3 mb-2" style="float:right;">
                                        <label for="inputPassword2" class="sr-only">Pilih kontes</label>
                                        <select class="form-control form-control-user" id="kontes" name="kontes">
                                            <option value="" selected>--Pilih kontes--</option>
                                        @foreach ($daftar_kontes as $kontes)
                                          @if ($kontes->status_kontes == 1)
                                              <option value="{{ $kontes->id }}">{{$kontes->nama_kontes}}</option>
                                          @endif
                                        @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user mb-2">Lihat</button>
                                </form>
                                @if ($kontes_dipilih == null)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Tahapan Kontes</th>
                                                <th scope="col">Tanggal berlangsung</>
                                                <th scope="col">Harga</>
                                                <th scope="col">Status</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <tr>
                                                  <td class="text-center" colspan="6">
                                                      Silakan pilih kontes yang ingin anda ikuti.
                                                  </td>
                                              </tr>
                                          </tbody>
                                    </table>
                                </div>
                            @else
                                <h3 class="h4 text-gray-900 mb-4 justify-content-center">{{$kontes_dipilih->nama_kontes}}</h3>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Tahapan saat ini</th>
                                            <th scope="col">Tanggal mulai</>
                                            <th scope="col">Tanggal berakhir</>
                                            <th scope="col">Harga</>
                                            <th scope="col">Status</th>
                                            <th scope="col">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                                <td>{{$kontes_dipilih->belongsto_tahapan->nama_tahapan}}</td>
                                                <td>{{date_format(date_create($kontes_dipilih->tgl_kontes),"d F Y")}}</td>
                                                <td>dd/mm/yyy</td>
                                                <td>{{$kontes_dipilih->belongsto_tahapan->harga_kontes}}</td>
                                                @if ($kontes_dipilih->belongsto_tahapan->status_tahapan == 1)
                                                <td>Aktif</td>
                                                <td>  
                                                    <a class="btn btn-primary btn-user mb-2" href="" data-toggle="modal" data-target="#registrasi">
                                                        Ikuti
                                                    </a>
                                                </td>
                                                @else
                                                <td>Tidak Aktif</td>
                                                @endif
                                        </tbody>
                                    </table>
                                </div>
                            @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('User.modal.modal-pilih-kontes')
@endsection

