<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Kartu</title>
</head>
<body style="margin:auto;">
    @foreach($data['nama_peserta'] as $key=>$list)
    <div style='background-image:url("{{Storage::url("kartu-peserta/kartu_peserta.png")}}");background-repeat:no-repeat; background-size: 185mm 275mm;width: 18.5cm;
    height: 27.5cm;background-position:50%;margin-top:20%;'>
        <table class="table" style="margin:auto;">
            <tr style="">
                <td>Asal Sekolah/Univ</td>
                <td>: {{$data['asal_sekolah']}}</td>
                <td></td>
            </tr>
            <tr>
                <td>Kota</td>
                <td>: {{$data['asal_kota']}}</td>
                <td></td>
            </tr>
            <tr>
                <td>No. Tim</td>
                <td>: {{$purchase->kode_peserta}}</td>
                <td></td>
            </tr>
            <tr>
                <td><br></td>
            </tr>

            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td>Nama Peserta {{$key+1}}</td>
                <td>: {{$data['nama_peserta'][$key]}}</td>
            </tr>
            <tr>
                <td>Tempat Lahir</td>
                <td>: {{$data['tempat_lahir'][$key]}}</td>
                <td></td>
            </tr>
            <tr>
                <td>Tgl Lahir</td>
                <td>: {{$data['tgl_lahir'][$key]}}</td>
                <td></td>
            </tr>
            <tr>
                <td>No.Tlp Peserta</td>
                <td>: {{$data['no_telepon'][$key]}}</td>
                <td></td>
            </tr>
            <tr>
                <td>Berkas {{$key+1}}</td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td><img width="250" src="{{Storage::url($data['foto_peserta'][$key])}}"></td>
            </tr>
        </table>
    </div>  
    @endforeach
</body>
</html>
