@extends('User-Layouts.master')

@section('title', 'Dashboard')

@section('content')
   <!-- Page Heading -->
   <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">DASHBOARD</h1>

</div>

<!-- Content Row -->
<div class="row">
    @foreach ($all_kontes as $kontes)
    <div class="col-xl-4 col-md-6 mb-4">
        <!-- Illustrations -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{$kontes->nama_kontes}}</h6>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                        src="img/undraw_posting_photo.svg" alt="">
                </div>
                <p>{{$kontes->deskripsi_kontes}}</p>
                <a target="_blank" rel="nofollow" href="https://undraw.co/">Lihat selengkapnya &rarr;</a>
            </div>
        </div>
    </div>
    @endforeach
    </div>
</div>
@endsection

