<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta
			name="viewport"
			content="width=device-width, initial-scale=1, shrink-to-fit=no"
		/>
		<meta name="description" content="" />
		<meta name="author" content="" />

		@section('title', 'Login')

		<!-- Custom fonts for this template-->
		<link
			href="vendor/fontawesome-free/css/all.min.css"
			rel="stylesheet"
			type="text/css"
		/>
		<link
			href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
			rel="stylesheet"
		/>

		<!-- Custom styles for this template-->
		<link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet" />
	</head>

	<body class="">
            {{-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">

                    </li>
                </ul>
            </nav> --}}
            <!-- End of Topbar -->

            <div class="container" style="margin-top: 5%;">
			<a href="/">
				<img src="images/logo.png" style="width:20%;margin:auto;display:flex;align-items:center;" alt="Logo Campuspedia">
			</a>
			<!-- Outer Row -->
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-8 col-md-9">
					<div class="card o-hidden border-0 shadow-lg my-5">
						<div class="card-body p-0">
							<!-- Nested Row within Card Body -->
							<div class="row">
								<!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
								<div class="col-lg">
									<div class="p-5">
										<div class="text-center">
											<h1 class="h4 text-gray-900 mb-4">FORM LOGIN</h1>
										</div>
										@if ($message = Session::get('message'))
										<div class="alert alert-danger alert-block">
											<button type="button" class="close" data-dismiss="alert">×</button>	
											<strong>{{ $message }}</strong>
										  </div>										
										@endif

										<form class="user" method="POST" action="{{ route('login') }}">
                                            @csrf
											<div class="form-group">
												<input
													type="email"
													class="form-control form-control-user"
                                                    name="email"
													id="exampleInputEmail"
													aria-describedby="emailHelp"
													placeholder="Enter Email Address..."
												/>
												@if ($errors->has('email'))
												<span class="help-block">
													<strong>{{ $errors->first('email') }}</strong>
												</span>
											@endif
											</div>
											<div class="form-group">
												<input
													type="password"
                                                    name="password"
													class="form-control form-control-user"
													id="exampleInputPassword"
													placeholder="Password"
												/>
												@if ($errors->has('password'))
												<span class="help-block">
													<strong>{{ $errors->first('password') }}</strong>
												</span>
												@endif
											</div>
											<button
                                                type="submit"
												class="btn btn-primary btn-user btn-block"
                                                style="font-weight: 800;font-size:16px;"
											>
												LOGIN
											</button>
										</form>
										<hr />
										<div
											class="row flex align-center justify-content-between"
										>
											<a class="medium" href="forgot-password.html"
												>Lupa password?</a
											>
											<a class="medium" href="{{ route('registrasi') }}"
												>Belum memiliki akun?</a
											>
										</div>
										<div class="text-center"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            </div>

		<!-- Bootstrap core JavaScript-->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!-- Core plugin JavaScript-->
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

		<!-- Custom scripts for all pages-->
		<script src="js/sb-admin-2.min.js"></script>

		@include('sweetalert::alert')
	</body>
</html>
