<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
     <!-- The above meta tags *must* come first in the head -->

    <!-- SITE TITLE -->
    <title>Campuspedia | Olimpiade Campuspedia</title>
    <meta name="description" content="Website Campuspedia">
    <meta name="keywords" content="Olimpiade, Event, Conference, Academic, School, Scholar" />
    <meta name="author" content="www.campuspedia.id">

    <!-- twitter card starts from here, if you don't need remove this section -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@yourtwitterusername" />
    <meta name="twitter:creator" content="@yourtwitterusername" />
    <meta name="twitter:url" content="http://yourdomain.com/" />
    <meta name="twitter:title" content="Your home page title, max 140 char" />
    <!-- maximum 140 char -->
    <meta name="twitter:description" content="Your site description, maximum 140 char " />
    <!-- maximum 140 char -->
    <meta name="twitter:image" content="{{asset('images/twittercardimg/twittercard-280-150.jpg')}}" />
    <!-- when you post this page url in twitter , this image will be shown -->
    <!-- twitter card ends from here -->

    <!-- facebook open graph starts from here, if you don't need then delete open graph related  -->
    <meta property="og:title" content="Olimpiade CAMPUSPEDIA" />
    <meta property="og:url" content="https://campuspedia.id" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:site_name" content="Campuspedia Website" />
    <!--meta property="fb:admins" content="" /-->
    <!-- use this if you have  -->
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{asset('assets/img/opengraph/fbphoto.jpg')}}" />
    <!-- when you post this page url in facebook , this image will be shown -->
    <!-- facebook open graph ends from here -->

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&display=swap" rel="stylesheet">
    <link href="{{asset('landing-page/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('landing-page/css/fontawesome-all.css')}}" rel="stylesheet">
    <link href="{{asset('landing-page/css/swiper.css')}}" rel="stylesheet">
	<link href="{{asset('landing-page/css/magnific-popup.css')}}" rel="stylesheet">
	<link href="{{asset('landing-page/css/styles.css')}}" rel="stylesheet">

	<!-- Favicon  -->
    <link rel="icon" href="{{asset('landing-page/images/ori.png')}}">
</head>
<body data-spy="scroll" data-target=".fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark">
        <div class="container">

            <!-- Text Logo - Use this if you don't have a graphic logo -->
            <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Gemdev</a> -->

            <!-- Image Logo -->
            <a class="navbar-brand" href="index.html"><img src="{{asset('landing-page/images/ori.png')}}" alt="alternative" style="width: 100%;height:auto;"></a>

            <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#details" style="font-size: 18px">Informasi <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#features" style="font-size: 18px">Pengumuman</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#contact" style="font-size: 18px">Kontak</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="/login" style="font-size: 18px">Login</a>
                    </li>
                </ul>

            </div> <!-- end of navbar-collapse -->
        </div> <!-- end of container -->
    </nav> <!-- end of navbar -->
    <!-- end of navigation -->


    <!-- Header -->
    <div class="header">
        <div class="ocean">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>
        <div class="container">
            <div class="row" style="display: flex; justify-content:center; flex-direction:column; width:100%;">
                <div class="col-lg" >
                    <div class="text-container" style="text-align:center;">
                        <h1 class="h1-large">SELAMAT DATANG</h1>
                        <h3 class="p-large">CALON PESERTA OLIMPIADE CAMPUSPEDIA</h3>
                        <a class="btn-solid-lg page-scroll" href="/registrasi">DAFTARKAN DIRI</a>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->

            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of header -->


    <!-- Statement -->
    <div id="statement" class="basic-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-container">
                        <h3>Tentang Olimpiade</h3>
                        <p class="p-medium text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of statement -->

    @php
        $id = 2;
    @endphp
    @foreach ($kontes as $data)
        @if (($loop->iteration % 2) != 0)
            {{$id = 3}}
            <!-- Details 1 -->
            <div id="details" class="basic-{{$id}}">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xl-7">
                            <div class="image-container">
                                <img class="img-fluid" src="{{asset('landing-page/images/details-1.svg')}}" alt="alternative">
                            </div> <!-- end of image-container -->
                        </div> <!-- end of col -->
                        <div class="col-lg-6 col-xl-5">
                            <div class="text-container">
                                <h2>{{$data->nama_kontes}}</h2>
                                <p class="text-justify">{{$data->deskripsi_kontes}}</p>
                                {{-- <ul class="list-unstyled li-space-lg">
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body"><strong>For startups</strong> which have less data and operations</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body"><strong>For big companies</strong> with a lot of data and daily inputs</div>
                                    </li>
                                </ul> --}}
                                <a class="btn-solid-reg popup-with-move-anim" href="#details-lightbox">Details</a>
                            </div> <!-- end of text-container -->
                        </div> <!-- end of col -->
                    </div> <!-- end of row -->
                </div> <!-- end of container -->
            </div> <!-- end of basic-2 -->
            <!-- end of details 1 -->
        @else
            {{$id = 2}}
            <div class="basic-{{$id}}">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xl-5">
                            <div class="text-container">
                                <h2>{{$data->nama_kontes}}</h2>
                                <p class="text-justify">{{$data->deskripsi_kontes}}</p>
                                {{-- <ul class="list-unstyled li-space-lg">
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body"><strong>For startups</strong> which have less data and operations</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body"><strong>For big companies</strong> with a lot of data and daily inputs</div>
                                    </li>
                                </ul> --}}
                                <a class="btn-solid-reg popup-with-move-anim" href="#details-lightbox">Details</a>
                            </div> <!-- end of text-container -->
                        </div> <!-- end of col -->
                        <div class="col-lg-6 col-xl-7">
                            <div class="image-container">
                                <img class="img-fluid" src="{{asset('landing-page/images/details-2.svg')}}" alt="alternative">
                            </div> <!-- end of image-container -->
                        </div> <!-- end of col -->
                    </div> <!-- end of row -->
                </div> <!-- end of container -->
            </div> <!-- end of basic-2 -->
            <!-- end of details 1 -->
        @endif
    @endforeach
    


    <!-- Details Lightbox -->
    <!-- Lightbox -->
	<div id="details-lightbox" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
			<div class="col-lg-8">
                <div class="image-container">
                    <img class="img-fluid" src="{{asset('landing-page/images/details-lightbox.jpg')}}" alt="alternative">
                </div> <!-- end of image-container -->
			</div> <!-- end of col -->
			<div class="col-lg-4">
                <h3>Deskripsi</h3>
				<hr>
                <p>The app can easily help you track your personal development evolution if you take the time to set it up.</p>
                <h4>Timeline</h4>
                <ul class="list-unstyled li-space-lg">
                    <li class="media">
                        <i class="fas fa-circle"></i><div class="media-body">Tahap 1 (08 Mei 2021)</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-circle"></i><div class="media-body">Tahap 2 (15 Mei 2021)</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-circle"></i><div class="media-body">Tahap 3 (16 Mei 2021)</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-circle"></i><div class="media-body">Tahap 4 (30 Mei 2021)</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-circle"></i><div class="media-body">Tahap 5 (5 April 2021)</div>
                    </li>
                </ul>
                <a class="btn-solid-reg mfp-close page-scroll" href="/registrasi">Daftar</a> 
                <button class="btn-outline-reg mfp-close as-button" type="button">Kembali</button>
			</div> <!-- end of col -->
		</div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->
    <!-- end of details lightbox -->

    <!-- Features -->
    {{-- <div id="features" class="basic-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="text-box bg-gray">
                        <i class="fas fa-rocket"></i>
                        <h4>Web framework</h4>
                        <p>Gemdev is a young and popular web framework designed to help users build websites and web apps in half the time it would take without it</p>
                    </div> <!-- end of text-box -->
                </div> <!-- end of col -->
                <div class="col-lg-4">
                    <div class="text-box bg-gray">
                        <i class="fas fa-images"></i>
                        <h4>Unlimited components</h4>
                        <p>Browse the enormous component collection and find the right parts for your online project. Cards, lists, image sliders, everything you need</p>
                    </div> <!-- end of text-box -->
                </div> <!-- end of col -->
                <div class="col-lg-4">
                    <div class="text-box bg-gray">
                        <i class="fas fa-download"></i>
                        <h4>Easy to download</h4>
                        <p>It's very easy to download Gemdev just login with your credentials and click the green download button. The package will download instantly</p>
                    </div> <!-- end of text-box -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-4">
                    <div class="text-box bg-gray">
                        <i class="fas fa-cog"></i>
                        <h4>Simple to setup</h4>
                        <p>With basic coding skills you can configure Gemdev down to the smallest detail. No more tutorials or browsing through knowledge bases</p>
                    </div> <!-- end of text-box -->
                </div> <!-- end of col -->
                <div class="col-lg-4">
                    <div class="text-box bg-gray">
                        <i class="fas fa-file-alt"></i>
                        <h4>Simple licensing</h4>
                        <p>We hear your pain and we're offering Gemdev under the MIT license, which means you can use it for any kind of project and for any number of times</p>
                    </div> <!-- end of text-box -->
                </div> <!-- end of col -->
                <div class="col-lg-4">
                    <div class="text-box bg-gray">
                        <i class="fas fa-award"></i>
                        <h4>Great results</h4>
                        <p>A lot of users have sent us positive feedback about the framework. That means we are on to something good here so we'll keep improving it</p>
                    </div> <!-- end of text-box -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-4 -->
    <!-- end of features --> --}}

    <!-- Statistics -->
    <div class="counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <!-- Counter -->
                    <div id="counter">
                        <div class="cell">
                            <div class="counter-value number-count" data-count="231">1</div>
                            <p class="counter-info">Happy Users</p>
                        </div>
                        <div class="cell">
                            <div class="counter-value number-count" data-count="385">1</div>
                            <p class="counter-info">Issues Solved</p>
                        </div>
                        <div class="cell">
                            <div class="counter-value number-count" data-count="159">1</div>
                            <p class="counter-info">Good Reviews</p>
                        </div>
                        <div class="cell">
                            <div class="counter-value number-count" data-count="127">1</div>
                            <p class="counter-info">Case Studies</p>
                        </div>
                        <div class="cell">
                            <div class="counter-value number-count" data-count="211">1</div>
                            <p class="counter-info">Orders Received</p>
                        </div>
                    </div>
                    <!-- end of counter -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of counter -->
    <!-- end of statistics -->


    {{-- <!-- Invitation -->
    <div class="basic-6">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-container bg-gray">
                        <h4>Gemdev is a well established web development framework dedicated to creatives and makers</h4>
                        <a class="btn-solid-lg" href="#your-link">Details</a>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-6 -->
    <!-- end of invitation --> --}}


    <!-- Contact -->
    <div id="contact" class="form-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h2-heading">Contact details</h2>
                    <p class="p-heading">Don't hesitate to send your questions through the contact form or <a class="blue no-line" href="mailto:admin@campuspedia.com">admin@campuspedia.com</a></p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">

                    <!-- Contact Form -->
                    <div class="form-container">
                        <img class="decoration" src="{{asset('landing-page/images/contact-envelope.svg')}}" alt="alternative">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control-input" id="cname" required>
                                <label class="label-control" for="cname">Name</label>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control-input" id="cemail" required>
                                <label class="label-control" for="cemail">Email</label>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control-textarea" id="cmessage" required></textarea>
                                <label class="label-control" for="cmessage">Your message</label>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="form-control-submit-button">Submit Message</button>
                            </div>
                        </form>
                    </div> <!-- end of form-container -->
                    <!-- end of contact form -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form-1 -->
    <!-- end of contact -->


    <!-- Footer -->
    <div class="footer bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="social-container">
                        <span class="fa-stack">
                            <a href="https://www.facebook.com/campuspedia" target="_blank">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="https://twitter.com/campuspedia_id" target="_blank">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="https://page.line.me/gso5731t?openQrModal=true" target="_blank">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-line fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="https://www.instagram.com/campuspedia/" target="_blank">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="https://www.youtube.com/channel/UCPGrmZDHa5W4lCEudr8yYkA" target="_blank">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-youtube fa-stack-1x"></i>
                            </a>
                        </span>
                    </div> <!-- end of social-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of footer -->
    <!-- end of footer -->


    <!-- Copyright -->
    <div class="copyright bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © <a class="no-line" href="#your-link">2021. All rights reserved</a></p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright -->
    <!-- end of copyright -->


    <!-- Scripts -->
    <script src="{{asset('landing-page/js/jquery.min.js')}}"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="{{asset('landing-page/js/bootstrap.min.js')}}"></script> <!-- Bootstrap framework -->
    <script src="{{asset('landing-page/js/jquery.easing.min.js')}}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="{{asset('landing-page/js/swiper.min.js')}}"></script> <!-- Swiper for image and text sliders -->
    <script src="{{asset('landing-page/js/jquery.magnific-popup.js')}}"></script> <!-- Magnific Popup for lightboxes -->
    <script src="{{asset('landing-page/js/scripts.js')}}"></script> <!-- Custom scripts -->
</body>
</html>
