<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="description" content="">
		<meta name="author" content="">

		@section('title', 'Registrasi')

		 <!-- Custom fonts for this template-->
		 <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		 <link
			 href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
			 rel="stylesheet">
	 
		 <!-- Custom styles for this template-->
		 <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
		 <link href="{{asset('css/custom.css')}}" rel="stylesheet">
	</head>

	<body class="">
        {{-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            <!-- Sidebar Toggle (Topbar) -->
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
            </button>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">
                <div class="topbar-divider d-none d-sm-block"></div>

                <!-- Nav Item - User Information -->
                <li class="nav-item dropdown no-arrow">

                </li>
            </ul>
        </nav> --}}
        <!-- End of Topbar -->
		<div class="container" style="margin-top: 5%;">
			<a href="/">
				<img src="images/logo.png" style="width:20%;margin:auto;display:flex;align-items:center;" alt="Logo Campuspedia">
			</a>
			<!-- Outer Row -->
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-8 col-md-9">
					<div class="card o-hidden border-0 shadow-lg my-5">
						<div class="card-body p-0">
							<!-- Nested Row within Card Body -->
							<div class="row">
								<!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
								<div class="col-lg">
									<div class="p-5">
										<div class="text-center">
											<h1 class="h4 text-gray-900 mb-4">FORM REGISTRASI</h1>
										</div>
										<form class="user" id="registrasi-form" action="{{route('registrasi')}}" method="POST">
                                            {{ csrf_field() }}
											<div class="form-group">
												<input
													type="email"
                                                    name="email"
													class="form-control form-control-user"
													id="email"
													placeholder="Email ..."
												/>
                                                <span class="text-danger"  style="font-size:13px">{{ $errors->first('email') }}</span>
											</div>

											<div class="form-group">
												<input
													type="password"
                                                    name="password"
													class="form-control form-control-user"
													id="password"
													placeholder="Password"
												/>
                                                <span class="text-danger"  style="font-size:13px">{{ $errors->first('password') }}</span>
											</div>
                                            <div class="form-group">
												<input
													type="password"
                                                    name="confirm_password"
													class="form-control form-control-user"
													id="confirm_password"
													placeholder="Password Confirmation"
												/>
                                                <span class="text-danger"  style="font-size:13px">{{ $errors->first('confirm_password') }}</span>
											</div>
											<div class="form-group">
												<button
													type="submit"
													class="btn btn-primary btn-user btn-block"
													style="font-size:16px;font-weigth:800;"
												>
													REGISTRASI
												</button>
											</div>
										</form>
										<hr />
										<div
											class="col-lg flex align-center justify-content-between"
										>
											<a class="medium" href="{{route('login')}}"
												>Sudah memiliki akun? </a
											>
										</div>
										<div class="text-center"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		 <!-- Bootstrap core JavaScript-->
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		 <script src="vendor/jquery/jquery.min.js"></script>
		 <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	 
		 <!-- Core plugin JavaScript-->
		 <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	 
		 <!-- Custom scripts for all pages-->
		 <script src="js/sb-admin-2.min.js"></script>
	 
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
		@include('sweetalert::alert')

		 <script>
			 if ($("#registrasi-form").length > 0) {
				$("#registrasi-form").validate({
					rules: {
						email: {
							required: true,
							maxlength: 50,
							email: true,
						},
						password: {
							required: true,
							minlength:8,
							maxlength: 50,
						},
						confirm_password: {
							required: true,
							minlength:8,
							maxlength: 50,
							equalTo:"#password",
						}
					},
					messages: {
						email: {
							required: "Form perlu diisi",
							maxlength: "Email tidak boleh lebih dari 50 karakter",
							email: "Email tidak valid",
						},
						password: {
							required: 'Form perlu diisi',
							minlength:'Password tidak boleh kurang dari 8 karakter',
							maxlength: 'Password tidak boleh lebih dari 50 karakter',
						},
						confirm_password: {
							required: 'Form perlu diisi',
							minlength:'Password tidak boleh kurang dari 8 karakter',
							maxlength: 'Password tidak boleh lebih dari 50 karakter',
							equalTo:"Password tidak cocok",
						}
					},
				})
			 }
        </script>
	</body>
</html>