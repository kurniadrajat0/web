@extends('templates.t_back_user')
@section('title','Cetak Kartu')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->

        <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h3 class="m-0 font-weight-bold text-primary">Cetak Kartu</h3>

                    </div>
                    <!-- Card Body -->
                    @include('flash::message')
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img height="100" src="{{asset('front/images/home/logo_its.png')}}">
                            </div>
                            <div class="col-md-8 text-center">
                                <h4>KARTU PESERTA EPW 2021</h4>
                                <h4>INSTITUT TEKNOLOGI SEPULUH NOPEMBER</h4>
                            </div>
                            <div class="col-md-2">
                                <img height="100" src="{{asset('front/images/home/logo_epc.png')}}">
                            </div>
                        </div>
                        <hr>
                        <div class="row">

                            <div class="col-md-12 box-praregis">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Asal Sekolah/Univ</td>
                                        <td>{{$data['asal_sekolah']}}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Kota</td>
                                        <td>{{$data['asal_kota']}}</td>
                                        <td></td>
                                    </tr>
                                    
                                    @foreach($data['nama_peserta'] as $key=>$list)
                                        <tr>
                                            <td>Nama Peserta {{$key+1}}</td>
                                            <td>{{$data['nama_peserta'][$key]}}</td>
                                        </tr>
                                        <tr>
                                            <td>NISN/NIM {{$key+1}}</td>
                                            <td>{{$data['nim_peserta'][$key]}}</td>
                                        </tr>
                                       
                                        <tr>
                                            <td>Tempat Lahir {{$key+1}}</td>
                                            <td>{{$data['tempat_lahir'][$key]}}</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Tgl Lahir {{$key+1}}</td>
                                            <td>{{$data['tgl_lahir'][$key]}}</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat {{$key+1}}</td>
                                            <td>{{$data['alamat_peserta'][$key]}}</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>No.Tlp Peserta {{$key+1}}</td>
                                            <td>{{$data['no_tlp'][$key]}}</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Email Peserta {{$key+1}}</td>
                                            <td>{{$data['email_peserta'][$key]}}</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Foto Peserta {{$key+1}}</td>
                                            <td><img height="150" src="{{url('/uploads/photos/'.$data['foto_peserta'][$key])}}"></td>
                                        </tr>
                                        <tr>
                                            <td>Kartu Pelajar {{$key+1}}</td>
                                            <td><img height="250" src="{{url('/uploads/photos/'.$data['foto_kartu'][$key])}}"></td>
                                        </tr>
                                    @endforeach
                                </table>
                                <div class="form-group">
                                    <form class="form" action="{{URL::to('/peserta/cetak_kartu')}}" method="post">
                                        {{ csrf_field() }}
                                        <input hidden name="kontes_id" value="{{$kontes_id}}">
                                        <button type="submit" class="btn btn-primary">Print</button>
                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->



@stop