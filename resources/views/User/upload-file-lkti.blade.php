@extends('User-Layouts.master')

@section('title', 'Upload File LKTI')

@section('extracss')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
@endsection

@section('extrajs')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">UPLOAD FILE LKTI</h1>
</div>

<div class="container-fluid">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h3 class="h4 text-gray-900 mb-4">UPLOAD DOKUMEN ANDA</h3>
                                </div>
                                <!-- Head of your HTML file -->
                                <form class="user" id="lkti-form" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="peserta_id" id="peserta_id" value="{{$peserta->id}}">
                                    <input type="hidden" name="kontes_id" id="kontes_id" value="{{$kontes->id}}">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Upload File</label>
                                        <input class="form-control form-control-user" type="file" name="file_lkti" id="file_lkti" onchange="return fileValidation()" accept=".doc,.docx,.pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Judul Karya Tulis</label>
                                        <input
                                            type="text"
                                            name="nama_media"
                                            class="form-control form-control-user"
                                            id="nama_media"
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Deskripsi</label>
                                        <textarea
                                        name="deskripsi_media"
                                        class="form-control"
                                        id="deskripsi_media"
                                        cols="40" rows="5"></textarea>
                                    </div>
                                    <button
                                        type="submit"
                                        name="submit-file"
                                        id="submit-file"
                                        class="btn btn-primary btn-user btn-block submit"
                                        style="font-weight: 800;font-size:16px;margin-top:20px;"
                                    >
                                        Submit
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    function fileValidation(){
        var fileInput = document.getElementById('file_lkti');
              
        var filePath = fileInput.value;
        
        // Allowing file type
        var allowedExtensions = /(\.doc|\.docx|\.pdf)$/i;
            
        if (!allowedExtensions.exec(filePath)) {
            swal({
                    title: "Terjadi kesalahan",
                    text: "Tipe file tidak sesuai ketentuan",
                    icon: "error",
                });
            fileInput.value = '';
            return false;
        } 
    }

    $(document).on('submit','#lkti-form',function(e){
        e.preventDefault();

        $('#submit-file').html('Menyimpan...');
        const file = $('#file_lkti').prop('files')[0];
        let formData = new FormData();
        formData.append('nama_media', $('#nama_media').val());
        formData.append('deskripsi_media', $('#deskripsi_media').val());
        formData.append('peserta_id', $('#peserta_id').val());
        formData.append('kontes_id', $('#kontes_id').val());
        formData.append('file', file);
        $.ajax({
            url: "{{ route('user.lkti-upload') }}",
            type: "POST",
            data: formData,
            dataType: 'json', 
            cache: false,
            processData: false,
            contentType: false,
            success:function(response){
                console.log(response);
                var title;
                var icon;
                var msg = response.message;
                if(response.status){
                    title = "Success";
                    icon = "success";
                } else {
                    title = "Gagal";
                    icon = "error";
                }
                swal({
                    title: title,
                    text: msg,
                    icon: icon,
                });
                $("#lkti-form")[0].reset();
                $('#submit-file').html('Submit');
            },
            error:function(response){
                console.log(response);
                swal({
                    title: "Gagal",
                    text: "File tidak berhasil disimpan",
                    icon: "error",
                });
                $("#lkti-form")[0].reset();
                $('#submit-file').html('Submit');
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        });
    });
</script>
@endsection

