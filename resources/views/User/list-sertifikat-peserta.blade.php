@extends('User-Layouts.master')

@section('title', 'Sertifikat Peserta')

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">SERTIFIKAT PESERTA</h1>
</div>

<div class="container-fluid">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h3 class="h4 text-gray-900 mb-4">Daftar Sertifikat Anda</h3>
                                </div>
                     
                                <div class="table-responsive">
                                    <table class="table" width="100%" cellspacing="0">
                                        <thead class="text-white bg-primary">
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Kontes</th>
                                                <th>Babak</th>
                                                <th>Tgl Pelaksanaan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $key => $val)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>
                                                    {{$val->nama_kontes}}
                                                </td>
                                                <td>
                                                    {{$val->nama_tahapan}}
                                                </td>
                                                <td>{{date_format(date_create($val->tgl_kontes),"d F Y")}}</td>
                                                <td><a class="btn btn-primary" href="{{URL::to('/sertifikat/download/'.($val->id))}}">Download</a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection