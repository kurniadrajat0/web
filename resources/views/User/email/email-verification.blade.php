<html>
    <head>
        <style type='text/css'>
        body {margin: 0; padding: 0; min-width: 100%!important;}
        .content {width: 100%; max-width: 400px;}
        </style>
    </head>
    <body yahoo bgcolor='#f6f8f1' style='padding-top:30px; padding-bottom:30px; background-color:#f6f8f1'>
        <table width='100%' bgcolor='#f6f8f1' border='0' cellpadding='0' cellspacing='0'>
            <tr>
                <td>
                    <table class='content' align='center' cellpadding='0' cellspacing='0' border='0'>
                        <tr>
                            <td style='width:100%; background-color:#333'>
                               <div class='box' style='display:block; margin-left:auto; margin-right:auto; background-color:#fff;'>
                                {{-- <img  src="{{URL::asset('img/email/email_verification.png')}}" style='width:80px' /> --}}
                                <div style=' background-color:#fff; padding: 20px'>
                                <h3>EMAIL VERIFICATION</h3>
                                <p>Hello, {{$user}},</p>
								<p>Kami siap mengaktifkan akun Anda. Yang perlu kami lakukan adalah memastikan ini adalah alamat email Anda.</p>
                                <br>
                                @if($type=='tentor')
                                <p>Terimakasih telah mendaftar menjadi Tentor Campuspedia Silahkan gabung dengan group WA Tentor untuk mendapat berbagai info di Campuspedia Edu melalui link berikut: <a href="https://bit.ly/2Yzvkwx">https://bit.ly/2Yzvkwx</a></p>
                                @endif
                                <p align='center'>
                                <a style='background-color: #4bb1bd; /* Green */
                                border: none;
                                color: white;
                                padding: 15px 32px;
                                text-align: center;
                                text-decoration: none;
                                display: inline-block;
                                margin-left:auto;
                                margin-right:auto;
                                font-weight:800;
                                font-size: 16px;' href="{{ URL::to('registrasi/verifikasi/'.$type.'/'.$confirmation_code) }}" target='_blank'>Verifikasi email saya</a></p>
                                <br>
                                <p>Jika tombol tidak bisa diklik silahkan copy link berikut dan paste di browser Anda</p>
                                <p><a href="{{ URL::to('registrasi/verifikasi/'.$type.'/'.$confirmation_code) }}">{{ URL::to('registrasi/verifikasi/'.$type.'/'.$confirmation_code) }}</a></p>
                                <p>Thanks,</p>
                                <p>Support Campuspedia Team</p>
                                <br>
                                </div></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
