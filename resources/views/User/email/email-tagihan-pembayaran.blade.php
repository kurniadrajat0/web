<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Invoice</title>
	<!-- Favicon  -->
	<link rel="icon" href="{{asset('landing-page/images/ori.png')}}">

	 <!-- Styles -->
	 <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&display=swap" rel="stylesheet">
	 <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&display=swap" rel="stylesheet">
	 <link href="{{asset('landing-page/css/bootstrap.css')}}" rel="stylesheet">
	 <link href="{{asset('landing-page/css/fontawesome-all.css')}}" rel="stylesheet">
	 <link href="{{asset('landing-page/css/swiper.css')}}" rel="stylesheet">
	 <link href="{{asset('landing-page/css/magnific-popup.css')}}" rel="stylesheet">
	 <link href="{{asset('landing-page/css/styles.css')}}" rel="stylesheet">

	<!-- Invoice styling -->
	<style>
	body{
		font-family:'Poppins', 'Open Sans', sans-serif;
		text-align:center;
		color:#777;
	}

	body h1{
		font-weight:300;
		margin-bottom:0px;
		padding-bottom:0px;
		color:#000;
	}

	body h3{
		font-weight:300;
		margin-top:10px;
		margin-bottom:20px;
		font-style:italic;
		color:#555;
	}

	body a{
		color:#06F;
	}

	.invoice-box{
		max-width:800px;
		margin:auto;
		padding:30px;
		border:1px solid #eee;
		box-shadow:0 0 10px rgba(0, 0, 0, .15);
		font-size:16px;
		line-height:24px;
		font-family:'Poppins', 'Open Sans', sans-serif;
		color:#555;
	}

	.invoice-box table{
		width:100%;
		line-height:inherit;
		text-align:left;
	}

	.invoice-box table td{
		padding:5px;
		vertical-align:top;
	}

	.invoice-box table tr td:nth-child(2){
		text-align:right;
	}

	.invoice-box table tr.top table td{
		padding-bottom:20px;
	}

	.invoice-box table tr.top table td.title{
		font-size:45px;
		line-height:45px;
		color:#333;
	}

	.invoice-box table tr.information table td{
		padding-bottom:40px;
	}

	.invoice-box table tr.heading td{
		background:#eee;
		border-bottom:1px solid #ddd;
		font-weight:bold;
	}

	.invoice-box table tr.details td{
		padding-bottom:20px;
	}

	.invoice-box table tr.item td{
		border-bottom:1px solid #eee;
	}

	.invoice-box table tr.item.last td{
		border-bottom:none;
	}

	.invoice-box table tr.total td:nth-child(2){
		border-top:2px solid #eee;
		font-weight:bold;
	}

	@media only screen and (max-width: 600px) {
		.invoice-box table tr.top table td{
			width:100%;
			display:block;
			text-align:center;
		}

		.invoice-box table tr.information table td{
			width:100%;
			display:block;
			text-align:center;
		}
	}
	</style>
</head>

<body>
	<div class="invoice-box">
		<table cellpadding="0" cellspacing="0">
			<tr class="top">
				<td colspan="4">
					<table>
						<tr>
							<td class="title" colspan="2">
                                <img src="{{asset('images/logo.png')}}"
                                style="width:100%; max-width:200px;">
							</td>
							<td class="float-right" colspan="2">
								Invoice: #{{$data->no_invoice}}<br>
								Dibuat: {{ $data->tanggal_transfer}}<br>
							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr class="information">
				<td colspan="4">
					<p>Terimaksih {{ $data->nama_lengkap }}, karena telah melakukan pendaftaran olimpiade Campuspedia. Berikut ini detail order Anda:</p>
				</td>
			</tr>
            <tr class="information">
                <td>
                    <p>Nomor invoice</p>
                </td>
				<td>:</td>
                <td class="float-right"><p>{{$data->no_invoice}}</p></td>
            </tr>
            <tr class="information">
                <td>
                    <p>Kontes yang diikuti</p>
                </td>
				<td> : </td>
                <td class="float-right"><p> {{$data->nama_kontes}}</p></td>
            </tr>
            <tr class="information">
				<td>
					<p>Deskripsi kontes</p>
                </td>
				<td> : </td>
                <td class="float-right"><p>{{$data->deskripsi_kontes}}</p></td>
            </tr>
            <tr class="information">
				<td>
					<p>Harga Kontes</p>
                </td>
				<td> : </td>
                <td class="float-right"><p> {{$data->harga_kontes}} </p></td>
            </tr>
            <tr class="information">
				<td>
					<p>Potongan voucher</p>
                </td>
				<td> : </td>
                <td class="float-right">
					@if ($data->potongan_voucher == null)
						<p> 0 </p>
					@else
						{{$data->potongan_voucher}}
					@endif
				</td>
            </tr>
            <tr class="information">
				<td>
					<p>Tagihan biaya (+ Kode Unik)</p>
                </td>
				<td> : </td>
                <td class="float-right"><p>Rp {{$data->total_tagihan}},- </p></td>
            </tr>
            <tr class="heading">
				<td colspan="4">
					Silahkan Transfer Ke:
				</td>
				<td>
				</td>
			</tr>
			<tr class="details">
				<td colspan="4"> 
					BCA <br> 014-004-7546 a.n Ahmad Zaenal Mustofa
				</td>
			</tr>
			<tr>
				<td class="py-4">
					Best regards,<br>
					Campuspedia Team
				</td>
			</tr>
		</table>
	</div>

</body>

<footer>
	<!-- Footer -->
	<div class="footer bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="social-container">
						<span class="fa-stack">
							<a href="#your-link">
								<i class="fas fa-circle fa-stack-2x"></i>
								<i class="fab fa-facebook-f fa-stack-1x"></i>
							</a>
						</span>
						<span class="fa-stack">
							<a href="#your-link">
								<i class="fas fa-circle fa-stack-2x"></i>
								<i class="fab fa-twitter fa-stack-1x"></i>
							</a>
						</span>
						<span class="fa-stack">
							<a href="#your-link">
								<i class="fas fa-circle fa-stack-2x"></i>
								<i class="fab fa-pinterest-p fa-stack-1x"></i>
							</a>
						</span>
						<span class="fa-stack">
							<a href="#your-link">
								<i class="fas fa-circle fa-stack-2x"></i>
								<i class="fab fa-instagram fa-stack-1x"></i>
							</a>
						</span>
						<span class="fa-stack">
							<a href="#your-link">
								<i class="fas fa-circle fa-stack-2x"></i>
								<i class="fab fa-youtube fa-stack-1x"></i>
							</a>
						</span>
					</div> <!-- end of social-container -->
				</div> <!-- end of col -->
			</div> <!-- end of row -->
		</div> <!-- end of container -->
	</div> <!-- end of footer -->
	<!-- end of footer -->


	<!-- Copyright -->
	<div class="copyright bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p class="p-small">Copyright © <a class="no-line" href="#your-link">2021. All rights reserved</a> <br> 
						You are receiving this email because you opted in via our website.</p>
				</div> <!-- end of col -->
			</div> <!-- enf of row -->
		</div> <!-- end of container -->
	</div> <!-- end of copyright -->
	<!-- end of copyright -->
</footer>

</html>
