@extends('User-Layouts.master')

@section('title', 'Pendaftaran Tim')

@section('extracss')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{-- <link rel="stylesheet" href="/resources/demos/style.css"> --}}
@endsection

@section('extrajs')
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">DAFTAR OLIMPIADE</h1>
</div>

<div class="container-fluid">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h3 class="h4 text-gray-900 mb-4">DAFTARKAN TIM</h3>
                                </div>
                                <form class="user" name="daftar_tim" id="daftar_tim" method="post" action="{{ route('user.daftarkan-tim') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="kontes_id" value="{{$kontes_id}}">
                                    <div class="row mb-4">
                                        <div class="col">
                                            <a
                                            class="btn btn-primary btn-user remove"
                                            id="remove-btn"
                                            style="font-weight: 800;font-size:16px; width:50px;"
                                            >
                                            -
                                            </a>

                                            <a
                                            class="btn btn-primary btn-user addRow"
                                            style="font-weight: 800;font-size:16px; width:50px;"
                                            >
                                            +
                                            </a>

                                            <button
                                            class="btn btn-primary btn-user"
                                            style="font-weight: 800;font-size:16px;"
                                            disabled
                                            >
                                            Anggota Tim
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col">
                                            <label for="exampleFormControlSelect1">Asal Sekolah</label>
                                          <input type="text" class="form-control form-control-user" name="asal_sekolah" value="{{$data['asal_sekolah']}}" @if($lock_data==2) {{'disabled="true"'}} @endif>
                                        </div>
                                        <div class="col">
                                            <label for="exampleFormControlSelect1">Asal Kota</label>
                                          <input type="text" class="form-control form-control-user" name="asal_kota" value="{{$data['asal_kota']}}" @if($lock_data==2) {{'disabled="true"'}} @endif>
                                        </div>
                                    </div>
                                    @php($awal=0)
                                    @foreach($data['nama_peserta'] as $ke=>$list)
                                    <div id="box-peserta">
                                        <div class="box-peserta" id="box-1">
                                            <strong>Anggota 1</strong>
                                            <div class="form-group">
                                              <label for="exampleFormControlSelect1">Nama Lengkap</label>
                                              <input type="text" class="form-control form-control-user" name="nama_peserta[{{$ke}}]" id="nama_peserta" value="{{$data['nama_peserta'][$ke]}}" @if($lock_data==2) {{'disabled="true"'}} @endif>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col">
                                                    <label for="exampleFormControlSelect1">Tempat Lahir</label>
                                                  <input type="text" class="form-control form-control-user" name="tempat_lahir[{{$ke}}]" value="{{$data['tempat_lahir'][$ke]}}" @if($lock_data==2) {{'disabled="true"'}} @endif>
                                                </div>
                                                <div class="col">
                                                    <label for="exampleFormControlSelect1">Tanggal Lahir</label>
                                                  <input type="date" class="form-control form-control-user" name="tgl_lahir[{{$ke}}]" value="{{$data['tgl_lahir'][$ke]}}" @if($lock_data==2) {{'disabled="true"'}} @endif>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Nomor Telepon</label>
                                                <input type="text" class="form-control form-control-user" name="no_telepon[{{$ke}}]" id="no_telepon" value="{{$data['no_telepon'][$ke]}}" @if($lock_data==2) {{'disabled="true"'}} @endif>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">ID Instagram</label>
                                                <input type="text" class="form-control form-control-user" name="instagram[{{$ke}}]" id="instagram" value="{{$data['instagram'][$ke]}}" @if($lock_data==2) {{'disabled="true"'}} @endif>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Upload Foto</label>
                                                <input
                                                    type="file"
                                                    name="foto_peserta[{{$ke}}]"
                                                    class="form-control form-control-user"
                                                    id="foto_peserta"
                                                    @if($lock_data==2) {{'disabled="true"'}} @endif
                                                />
                                            </div>
                                            <div class="form-group">
                                                <?php if(isset($data['foto_peserta'][$ke])){ ?>
                                                <img width="300" src="{{Storage::url($data['foto_peserta'][$ke])}}" />
                                                <?php } ?>
                                        </div>
                                    </div>
                                    @php($awal=$ke)
                                    @endforeach
                                    <button
                                        type="submit"
                                        name="submit"
                                        id="submit"
                                        class="btn btn-primary btn-user btn-block submit"
                                        style="font-weight: 800;font-size:16px;"
                                    >
                                    SUBMIT
                                </button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var sectionsCount = 1;
    $('.addRow').on('click', function(){
        if (sectionsCount < 3) {
            addRow();
        }
    });

    function addRow(){
        sectionsCount++;
        var section =   '<div id="box-peserta">'+
                        '<div class="box-peserta" id="box-' + sectionsCount + '">'+
                            '<strong>Anggota ' + sectionsCount + '</strong>' +
                            '<div class="form-group">' + 
                                '<label for="exampleFormControlSelect1">Nama Lengkap</label>' +
                                '<input type="text" class="form-control form-control-user" name="nama_peserta[]" id="nama_peserta" placeholder="Masukkan nama lengkap ...">' +
                            '</div>' +
                            '<div class="row mb-4">'+
                                '<div class="col">'+
                                    '<label for="exampleFormControlSelect1">Tempat Lahir</label>'+
                                    '<input type="text" class="form-control form-control-user" name="tempat_lahir[]" placeholder="Masukkan tempat lahir ...">'+                        
                                '</div>' +
                                '<div class="col">' +
                                    '<label for="exampleFormControlSelect1">Tanggal Lahir</label>'+
                                    '<input type="date" class="form-control form-control-user" name="tgl_lahir[]" placeholder="Masukkan tanggal lahir ...">'+
                                '</div>' +
                            '</div>'+
                            '<div class="form-group">'+
                                '<label for="exampleFormControlSelect1">Nomor Telepon</label>'+
                                '<input type="text" class="form-control form-control-user" name="no_telepon[]" id="no_telepon" placeholder="Masukkan nomor telepon ...">'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label for="exampleFormControlSelect1">ID Instagram</label>'+
                                '<input type="text" class="form-control form-control-user" name="instagram[]" id="instagram" placeholder="Masukkan id instagram ...">'+
                            '</div>' +
                            '<div class="form-group">'+
                                '<label for="exampleFormControlSelect1">Upload Foto</label>'+
                                ' <input ' +
                                        'type="file"'+
                                        'name="foto_peserta[]"'+
                                        'class="form-control form-control-user"'+
                                        'id="foto_peserta"'+
                                    '/>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

        $('#box-peserta').append(section);
    };

    $('#daftar_tim').on('click', '.remove' ,function() {
        var id = sectionsCount;
        $("#box-" + id).remove();
        sectionsCount--;

        return false;
    });


    $(document).on('submit','#daftar_tim', function(event){
        event.prevenDefault();
        var token =  $('input[name="csrfToken"]').attr('value');
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Csrf-Token', token);
            }
        });

        $.ajax({
            url : "/pendaftaran-tim",
            type : "POST",
            data : JSON.stringify($('#daftar_tim').serialize()),
            contentType : 'application/json',
            success:function(data){
                console.log(data);
                if(data) {
                    $("#daftar_tim")[0].reset();
                    window.location.replace("/dashboard");
                }
            },
            error:function(data){
                alert("error");
            }
        });
    });
</script>
@endsection

