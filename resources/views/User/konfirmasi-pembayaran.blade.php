@extends('User-Layouts.master')

@section('title', 'Konfirmasi Pembayaran')

@section('extracss')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{-- <link rel="stylesheet" href="/resources/demos/style.css"> --}}
@endsection

@section('extrajs')
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">DAFTAR OLIMPIADE</h1>
</div>

<div class="container-fluid">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl col-lg-8 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h3 class="h4 text-gray-900 mb-4">KONFIRMASI PEMBAYARAN</h3>
                                </div>
                                <form class="user" method="post" action="{{ route('user.konfirmasi-pembayaran') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                      <label for="exampleFormControlSelect1">Metode Pembayaran</label>
                                      <select class="form-control form-control-user" id="metode_pembayaran" name="metode_pembayaran">
                                        <option value="Metode A">Metode A</option>
                                        <option value="Metode B">Metode B</option>
                                        <option value="Metode C">Metode C</option>
                                      </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Tanggal Transfer</label>
                                        <input type="date" class="form-control form-control-user" name="tanggal_transfer" id="tanggal_transfer">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Upload Bukti Transfer</label>
                                        <input
                                            type="file"
                                            name="file_bukti"
                                            class="form-control form-control-user"
                                            id="file_bukti"
                                        />

                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Masukkan kode voucher (jika ada)</label>
                                        <input
                                            type="text"
                                            name="kode_voucher"
                                            class="form-control form-control-user"
                                            id="kode_voucher"
                                        />
                                    </div>
                                    <button
                                        type="submit"
                                        class="btn btn-primary btn-user btn-block"
                                        style="font-weight: 800;font-size:16px;"
                                    >
                                    SUBMIT
                                </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

