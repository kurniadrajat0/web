
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
   
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/dashboard">
        <div class="sidebar-brand-icon">
            <img src="images/logo.png" style="width:90%" alt="">
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <div class="nav-item profile-user flex-column justify-content-center py-2">
       
        @if ($peserta->url_photo_profile == null)
            <img class="img-profile rounded-circle" style="width:80%;height:auto;margin:auto;" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="">
        @else
            <img class="img-profile rounded-circle" style="width:80%;height:auto;margin:auto;" src="{{ Storage::url($peserta->url_photo_profile) }}"  alt="">                                                 
        @endif
        
        <div class="nav-link flex align-items-center text-center text-white">
            <span style="font-size: 1em;"><strong>{{ $peserta->nama_lengkap }}</strong></span>
            <div class="user-info mt-2">
                <a class="label label-info" href="{{route('user.profile')}}" style="border-radius:25px;cursor:pointer;color:white;text-decoration:none;">
                    <i class="fas fa-fw fa-user"></i> 
                    <span>Lihat Profil</span> 
                </a>
            </div>
        </div>
    </div>

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{route('user.dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Nav Item - Informasi Olimpiade -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Informasi Olimpiade</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{route('user.status-pendaftaran')}}">Status Pendaftaran</a>
                <a class="collapse-item" href="{{route('user.pengumuman')}}">Pengumuman</a>
                <a class="collapse-item" href="{{route('user.list-sertifikat')}}">Sertifikat Anda</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Tata Cara Pendaftaran -->
     <li class="nav-item">
        <a class="nav-link" href="{{route('user.tata-cara-pendaftaran')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>Tata Cara Pendaftaran</span></a>
    </li>


    <!-- Nav Item - Daftar Olimpiade -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('user.pilih-kontes')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>Daftar Olimpiade</span></a>
    </li>

    <!-- Nav Item - Kontak -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('user.kontak')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>Kontak</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->
