<!-- Modal -->
<div class="modal fade" id="data-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">View Peserta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
          
          <form id="pesertadata" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" placeholder="Username" id="username" name="username" value="" disabled>
            </div>
            <div class="form-group">
              <label class="form-label" for="customFile">Profile</label>
              <input id="image" class="form-control" type="file" name="image" accept="image/*" onchange="readURL(this);" disabled>
              <!--<input type="file" class="form-control" id="url_photo_profile" name="url_photo_profile" placeholder=" Profile" disabled />-->
            </div>
            <div class="form-group">
              <img src="{{ Storage::url($epeserta->url_photo_profile) }}" alt="Preview" class="form-group hidden" id="url_photo_profile" name="url_photo_profile" width="100" height="100">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" placeholder="Email" id="email" name="email" value=""disabled>
            </div>
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" placeholder="Nama Lengkap" id="nama_lengkap" name="nama_lengkap" value=""disabled>
            </div>
            <div class="form-group">
                <label>Tempat Lahir</label>
                <input type="text" class="form-control" placeholder="Tempat Lahir" id="tempat_lahir" name="tempat_lahir" value=""disabled>
            </div>
            <div class="form-group">
                <label>Tanggal Lahir</label>                   
                <input type="datetime" id="tanggal_lahir" name="tanggal_lahir" class="form-control" value=""disabled>
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" class="form-control" placeholder="Alamat" id="alamat" name="alamat" value=""disabled>
            </div>
            <div class="form-group">
                <label>Jenjang Pendidikan</label> 
                <select class="form-control" name="jenjang_pendidikan" value="" disabled><!-- tambahin multiple="" kalo error  -->
                  <option value="0">SMP</option>
                  <option value="1">SMA/SMK</option>
                  <option value="2">S1</option>
                </select>
            </div>
            <div class="form-group">
                <label>Instansi Pendidikan</label>
                <input type="text" class="form-control" placeholder="instansi_pendidikan" id="instansi_pendidikan" name="instansi_pendidikan" value="" disabled>
            </div>
            <div class="form-group">
                <label>No Handphone</label>
                <input type="text" class="form-control" placeholder="no_handphone" id="no_handphone" name="no_handphone" value="" disabled>
            </div>
            <div class="form-group">
                <label>Whatsapp</label>
                <input type="text" class="form-control" placeholder="whatsapp" id="whatsapp" name="whatsapp" value="" disabled>
            </div>
            <div class="form-group">
                <label>Instagram</label>
                <input type="text" class="form-control" placeholder="instagram" id="instagram" name="instagram" value="" disabled>
            </div>
            <!--<div class="form-group">
              <button type="submit" class="btn btn-primary"> Update Data</button>
            </div>-->
            <div class="modal-footer">
              <!--<input type="submit" value="Submit" id="submit" class="btn btn-sm btn-outline-danger py-0" style="font-size: 0.8em;">-->
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" id="submit" class="btn btn-primary btn-user">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>
