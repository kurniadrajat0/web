
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en"> 
<head>
  @include('Template.header')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Peserta Baru</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('beranda-admin')}}">Home</a></li>
              <li class="breadcrumb-item active">Data Peserta Baru</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card card-info card-outline">
            <div class="card-body">
                <table class="table table-bordered" id="datatable">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th> Username </th>
                      <th> Email </th>
                      <th> Nama Lengkap </th>
                      <th> Tempat Lahir </th>  
                      <th> Tanggal Lahir </th>
                      <th> Alamat Tinggal </th>
                      <th> Jenjang Pendidikan </th>
                      <th> Instansi Pendidikan </th>
                      <th> No Handphone </th>
                      <th> WhatsApp </th>
                      <th> Instagram</th>
                      <th> Aksi </th>
                    </tr>       
                  </thead>
                  <tbody>

                  </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    @include('Template.footer')
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

@include('Template.script')
@include('sweetalert::alert')
@include('Peserta.modal_peserta')
<script>
  $(document).ready(function() {
    $('#datatable').DataTable({
      processing:true,
      serverside:true,
      ajax:"{{route('ajax.getdpeserta')}}",
      columns:[
        {data:'id',name:'id'},
        {data:'username',name:'username'},
        {data:'email',name:'email'},
        {data:'nama_lengkap',name:'nama_lengkap'},
        {data:'tempat_lahir',name:'tempat_lahir'},
        {data:'tanggal_lahir',name:'tanggal_lahir'},
        {data:'alamat',name:'alamat'},
        {data:'jenjang_pendidikan',name:'jenjang_pendidikan'},
        {data:'instansi_pendidikan',name:'instansi_pendidikan'},
        {data:'no_handphone',name:'no_handphone'},
        {data:'whatsapp',name:'whatsapp'},
        {data:'instagram',name:'instagram'},
        {data:'aksi',name:'aksi'},
      ]
    });
  });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

$(document).ready(function () {

$.ajaxSetup({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
//buat update
$('body').on('click', '#submit', function (event) {
    event.preventDefault()
    var id = $("#id").val();
    var username = $("#username").val();
    var url_photo_profile=  $("#url_photo_profile").val();
    var email = $("#email").val();
    var nama_lengkap = $("#nama_lengkap").val();
    var tempat_lahir = $("#tempat_lahir").val();
    var jenjang_pendidikan = $("#jenjang_pendidikan").val();
    var instansi_pendidikan = $("#instansi_pendidikan").val();
    var no_handphone = $("#no_handphone").val();
    var whatsapp = $("#whatsapp").val();
    var instagram = $("#instagram").val();
   
    $.ajax({
      url: 'update-user/' + id,
      type: "POST",
      data: {
        id: id,
        username :username,
        url_photo_profile : url_photo_profile,
        email :email,
        nama_lengkap :nama_lengkap,
        tempat_lahir :tempat_lahir,
        tanggal_lahir :tanggal_lahir,
        jenjang_pendidikan :jenjang_pendidikan,
        instansi_pendidikan :instansi_pendidikan,
        no_handphone :no_handphone,
        whatsapp :whatsapp,
        instagram :instagram,

      },
      dataType: 'json',
      success: function (data) {
          
          $('#pesertadata').trigger("reset");
          $('#data-user').modal('hide');
          window.location.reload(true);
      }
  });
});
//buat preview edit data
$('body').on('click', '#editpeserta', function (event) {
    //var SITEURL = '{{URL::to('')}}';
    var SITEURL = '{{Storage::url('')}}';
    //var gambar = ;
    event.preventDefault();
    var id = $(this).attr("data-id");
    //edit-user src="{{ Storage::url($epeserta->url_photo_profile) }}"
    $.ajax( {
      url: 'edit-user/' + id,
      type: "GET",
      success: function (data) {
        $('#id').val(data.data.id);
        console.log(id);
        $('#username').val(data.data.username);
        $('#modal-preview').attr('src'+'public/image_profile/'.$id+data.data.url_photo_profile);
        $('#url_photo_profile').val(data.data.url_photo_profile);
          
         //$('select[name=url_photo_profile]').val(data.data.url_photo_profile);
         /*
           $filename   = '';
            $gambar     = $req->file('foto_profile');
            $id         = $req->input('id_peserta');
            $peserta    = Peserta::where('id',$id);
            if ($req->hasFile('foto_profile')) {
                $destinationPath    = 'public/image_profile/'.$id;
                $extension          = $gambar->getClientOriginalExtension();
                $filename           = 'profile_'. time() . $id . '.' . $extension;
                $urlFile            = $gambar->storeAs($destinationPath, $filename);
         */
         $('#email').val(data.data.email);
         $('#nama_lengkap').val(data.data.nama_lengkap);
         $('#tempat_lahir').val(data.data.tempat_lahir);
         $('#tanggal_lahir').val(data.data.tanggal_lahir);
         $('#alamat').val(data.data.alamat);
         $('select[name=jenjang_pendidikan]').val(data.data.jenjang_pendidikan);
         $('#instansi_pendidikan').val(data.data.instansi_pendidikan);
         $('#no_handphone').val(data.data.no_handphone);
         $('#whatsapp').val(data.data.whatsapp);
         $('#instagram').val(data.data.instagram);
      },
      error : function() {
        alert("Error");
      }
     })
});

}); 
</script>

</body>
</html>
