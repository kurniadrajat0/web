
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en"> 
<head>
  @include('Template.header')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Team Baru</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('beranda-admin')}}">Home</a></li>
              <li class="breadcrumb-item active">Data Team Baru</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card card-info card-outline">
            <div class="card-body">
                <table class="table table-bordered" id="datatable">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th> Username </th>
                      <th> Email </th>
                      <th> Nama Lengkap </th>
                      <th> Tempat Lahir </th>  
                      <th> Tanggal Lahir </th>
                      <th> Alamat Tinggal </th>
                      <th> Jenjang Pendidikan </th>
                      <th> Instansi Pendidikan </th>
                      <th> No Handphone </th>
                      <th> WhatsApp </th>
                      <th> Instagram</th>
                      <th> Aksi </th>

                    </tr>       
                  </thead>
                  <tbody>

                  </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    @include('Template.footer')
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

@include('Template.script')
@include('sweetalert::alert')
@include('Team.modal_team')
<script>
  $(document).ready(function() {
    $('#datatable').DataTable({
      processing:true,
      serverside:true,
      ajax:"{{route('ajax.getdteam')}}",
      columns:[
        {data:'id',name:'id'},
        {data:'username',name:'username'},
        {data:'email',name:'email'},
        {data:'nama_lengkap',name:'nama_lengkap'},
        {data:'tempat_lahir',name:'tempat_lahir'},
        {data:'tanggal_lahir',name:'tanggal_lahir'},
        {data:'alamat',name:'alamat'},
        {data:'jenjang_pendidikan',name:'jenjang_pendidikan'},
        {data:'instansi_pendidikan',name:'instansi_pendidikan'},
        {data:'no_handphone',name:'no_handphone'},
        {data:'whatsapp',name:'whatsapp'},
        {data:'instagram',name:'instagram'},
        {data:'aksi',name:'aksi'},
      ]
    });
  });
</script>
</body>
</html>