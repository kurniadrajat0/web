<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlimMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olim_media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_media')->nullable();
            $table->text('deskripsi_media')->nullable();
            $table->string('file_media')->nullable();
            $table->integer('status_media')->nullable()->default('0');
            $table->integer('lock_data')->nullable()->default('0');
            $table->integer('peserta_id')->nullable();
            $table->integer('kontes_id')->nullable();
            $table->timestamp('created_at')->useCurrent()->nullable();
            $table->timestamp('updated_at')->useCurrent()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olim_media');
    }
}
