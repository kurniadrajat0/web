<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type_login')->default('biasa');
            $table->tinyInteger('email_verification')->default(0);
            $table->integer('active')->default(0);
            $table->integer('notif')->default(0);
            $table->string('confirmation_code')->nullable();
            $table->string('token_google')->nullable();
            $table->string('referal')->nullable();
            $table->string('kode_referal_saya')->nullable();
            $table->string('api_token')->nullable();
            $table->rememberToken();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
