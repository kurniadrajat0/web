<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlimDataTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olim_data_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peserta_id')->nullable();
            $table->text('data_tim')->nullable();
            $table->integer('kontes_id')->nullable();
            $table->integer('status_data')->nullable()->default(0);
            $table->integer('status_pembayaran')->nullable()->default(0);
            $table->integer('status_lolos')->nullable()->default(0);
            $table->timestamp('created_at')->useCurrent()->nullable();
            $table->timestamp('updated_at')->useCurrent()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olim_data_teams');
    }
}
