<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePesertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesertas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type_login')->default('biasa');
            $table->tinyInteger('email_verification')->default(0);
            $table->integer('active')->default(0);
            $table->integer('notif')->default(0);
            $table->string('confirmation_code')->nullable();
            $table->string('token_google')->nullable();
            $table->string('referal')->nullable();
            $table->string('kode_referal_saya')->nullable();
            $table->string('api_token')->nullable();

            $table->string('username')->nullable();
            $table->string('nama_lengkap')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->dateTime('tanggal_lahir')->nullable();
            $table->string('alamat')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kota')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('jenjang_pendidikan')->nullable();
            $table->string('instansi_pendidikan')->nullable();

            $table->string('no_handphone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('line')->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('url_photo_profile')->nullable();

            $table->rememberToken();
            $table->timestamp('last_login')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesertas');
    }
}
