<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlimPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olim_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_invoice')->nullable();
            $table->integer('peserta_id')->nullable();
            $table->integer('kontes_id')->nullable();
            $table->date('tanggal_transaksi')->nullable();
            $table->tinyInteger('status_pembayaran')->nullable()->default(0);
            $table->double('unique_number')->nullable();
            $table->double('total_tagihan')->nullable();
            $table->string('metode_pembayaran')->nullable();
            $table->dateTime('tanggal_transfer')->nullable();
            $table->string('file_bukti')->nullable();
            $table->string('kode_voucher')->nullable();
            $table->string('potongan_voucher')->nullable();
            $table->string('kode_peserta')->nullable();
            $table->timestamp('created_at')->useCurrent()->nullable();
            $table->timestamp('updated_at')->useCurrent()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olim_purchases');
    }
}
