<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateKontesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_kontes');
            $table->integer('jenis_kontes');
            $table->string('deskripsi_kontes');
            $table->date('tgl_kontes');
            $table->bigInteger('tahapan_kontesid');//mulai dari sini ngambil dari table tahapan ,ini di convert ke nama tahapan
            $table->integer('harga_kontes');
            $table->string('link_sertifikat');
            $table->integer('status_kontes');//sampe sini
            $table->string('pengumuman_kontes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kontes');
    }
}
