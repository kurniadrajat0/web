<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTahapanKontesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahapan_kontes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_tahapan');
            $table->integer('harga_kontes');
            $table->date('tgl_tahapan');
            $table->string('link_sertifikat');
            $table->integer('status_tahapan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahapan_kontes');
    }
}
