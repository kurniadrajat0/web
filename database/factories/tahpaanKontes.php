<?php

use Illuminate\Support\Str;

$factory->define(App\tahapanKontes::class, function (Faker\Generator $faker) {
    return [
        'nama_tahapan' => $faker->sentence,
        'harga_kontes' => $faker->randomNumber ,
        'tgl_tahapan' => $faker->dateTimeBetween('+0 days', '+2 years'),
        'link_sertifikat' => $faker->sentence,
        'status_tahapan' => $faker->numberBetween(1, 0),

    ];
});

?>